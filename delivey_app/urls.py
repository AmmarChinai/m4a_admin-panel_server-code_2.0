"""delivey_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path

# from payment.views import payment_process
from user.views import login_user, user_logout, login_admin, home, dashboard, sample, settings_data, become_partner, \
    banner_data, send_email, send_sms, tnc, partner_tnc, driver_tnc, privacy_policy, help_view, subscribe

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_admin),
    path('send_sms/', send_sms),
    path('x/', sample),
    path('tnc/', tnc),
    path('partner_tnc/', partner_tnc),
    path('driver_tnc/', driver_tnc),
    # path('payment_process/', payment_process),
    path('banner_data/', banner_data),
    path('privacy_policy/', privacy_policy),
    path('help/', help_view),
    path('subscribe/', subscribe),
    path('logout/', user_logout),
    path('home/', dashboard),
    path('settings/', settings_data),
    path('become_partner/', become_partner),
    path('user/', include('user.urls')),
    path('category/', include('category.urls')),
    path('payment/', include('payment.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
