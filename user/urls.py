
from django.urls import path

from user.views import *

urlpatterns = [
    path('login/', login_user),
    path('login_with_password/', login_with_password),
    path('logout/', user_logout),
    path('otp_verify/', otp_verify),
    path('forgot_password/', forgot_password),
    path('resend_otp/', resend_otp),
    path('change_password/', change_password),
    path('delete/', delete),
    path('home/', home),
    path('search_vendor/', search_vendor),
    path('edit_avatar/', edit_avatar),
    path('user_profile/', user_profile),
    path('reset_password/', reset_password),
    path('user_list/', user_data),

    path('vendor_list/', vendor_data),
    path('add_vendor/', add_vendor),
    path('edit_vendor/<int:vendor_id>/', edit_vendor),
    path('view_vendor/<int:vendor_id>/', view_vendor),
    path('add_subcategory/<int:vendor_id>/', add_subcategory_vendor),
    path('vendors/', vendor_data_mobile),
    path('pending_orders/', pending_orders),
    path('vendors/<int:vendor_id>/orders/', vendor_orders),
    path('vendor/<int:vendor_id>/offers/', vendor_offer_mobile),
    path('offer_delete/', vendor_offer_delete),
    path('vendor/<int:vendor_id>/', vendor_details),
    # path('offers/<int:vendor_id>/', vendor_offer_for_user),

    path('view_executive/<int:executive_id>/', view_executive),
    path('executive/<int:executive_id>/', executive_details),

    path('option_list/', option_data),


    path('executive_list/', executive_data),
    path('add_executive/', add_executive),
    path('edit_executive/<int:executive_id>/', edit_executive),

    path('orders/', order_data),
    path('address_list/', address_data),

    path('cancel_order/<int:order_id>/', cancel_order),
    path('order/<int:order_id>/', order_details_mobile),

    path('executive/<int:executive_id>/orders/', executive_orders),
    path('executive/<int:executive_id>/status/', executive_status),

    path('admin_user_list/', admin_user_list),
    path('edit_admin_user/', edit_admin_user),
    path('user_exists/', user_exists),
    path('admin_change_password/', admin_change_password),

    path('change_category_status/', change_category_status),
    path('change_sub_category_status/', change_sub_category_status),
    path('change_user_status/', change_user_status),

    path('<int:vendor_id>/vendor_report/', vendor_report),
    path('<int:vendor_id>/vendor_report_download/', vendor_report_download),
    path('<int:executive_id>/executive_report/', executive_report),
    path('<int:executive_id>/executive_report_download/', executive_report_download),

    path('send_report/', send_report),


]