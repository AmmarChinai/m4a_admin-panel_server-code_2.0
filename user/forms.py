from django.forms import ModelForm
from rest_framework.exceptions import ValidationError

from user.models import VendorData, User, VendorCertificateData, ExecutiveData, OptionData, \
    ChargeSettingData
from django.contrib.auth.forms import UserCreationForm as DjangoUserCreationForm


class VendorCreationForm(ModelForm):
    class Meta(ModelForm):
        model = VendorData
        fields = ('shop_name', 'latitude', 'longitude', 'mobile', 'mobile2', 'start_time', 'end_time', 'meat4all_per',
                  'email', 'bank_name', 'sort_code', 'account_no', 'holder_name', 'payment')


class VendorCertificateCreationForm(ModelForm):
    class Meta(ModelForm):
        model = VendorCertificateData
        fields = ('name', 'start', 'end', 'vendor', 'file')


class ExecutiveCreationForm(ModelForm):
    class Meta(ModelForm):
        model = ExecutiveData
        fields = ('latitude', 'longitude', 'mobile2', 'v_name', 'dl_date', 'dl_no', 'email', 'de_charge_per', 'mode',
                  'insurance', 'bank_name', 'sort_code', 'account_no', 'holder_name', 'payment')


class OptionCreationForm(ModelForm):
    class Meta(ModelForm):
        model = OptionData
        fields = ('name', 'upto_3_mile', 'from_3to5_mile', 'from_5to10_mile', 'from_10to30_mile', 'supplier_fee',
                  'packing_charge', 'vat', 'cancel_charge', 'driver_charge_upto_3_mile', 'driver_charge_from_3to5_mile',
                  'driver_charge_from_5to10_mile', 'driver_charge_from_10to30_mile')


class OthersUserCreationForm(ModelForm):

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("This Mobile Number is already exists!")
        return username

    class Meta(DjangoUserCreationForm):
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
        )


class ChargeSettingsCreationForm(ModelForm):
    class Meta(ModelForm):
        model = ChargeSettingData
        fields = ('upto_3_mile', 'from_3to5_mile', 'from_10to30_mile', 'from_5to10_mile',
                  'packing_charge_per', 'cancel_charge', 'distance',
                  'tip_charge', 'driver_charge_upto_3_mile', 'driver_charge_from_3to5_mile',
                  'driver_charge_from_5to10_mile', 'driver_charge_from_10to30_mile')
