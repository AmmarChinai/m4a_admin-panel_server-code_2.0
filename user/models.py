import uuid

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.db import models
from django.utils.crypto import get_random_string

from delivey_app.managres import LocationManager


def random_string():
    return uuid.uuid4().hex[:8]


class User(AbstractUser):
    isMobileVerified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True, null=True, blank=True)
    is_email = models.BooleanField(default=True, null=True, blank=True)
    is_vendor = models.BooleanField(default=False)
    is_executive = models.BooleanField(default=False)
    is_user = models.BooleanField(default=False)
    is_profile_completed = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to='', null=True, blank=True)
    gcm_token = models.TextField(null=True)
    gender = models.CharField(max_length=20, null=True, blank=True)
    source = models.CharField(max_length=10, null=True)
    latitude = models.CharField(max_length=100, null=True)
    longitude = models.CharField(max_length=100, null=True)
    device_type = models.CharField(max_length=32, null=True, blank=True)
    stripe_customer_id = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.username


class AdminUserData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_category_access = models.BooleanField(default=False)
    is_sub_category_access = models.BooleanField(default=False)
    is_user_access = models.BooleanField(default=False)
    is_vendor_access = models.BooleanField(default=False)
    is_executive_access = models.BooleanField(default=False)
    is_option_access = models.BooleanField(default=False)
    is_order_access = models.BooleanField(default=False)
    is_admin_user_access = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return "%s" % self.user.username


class AddressData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    block = models.CharField(max_length=100, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    landmark = models.CharField(max_length=100, null=True, blank=True)
    latitude = models.CharField(max_length=100, null=True, blank=True)
    longitude = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.user.username


class OtpData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    code = models.PositiveIntegerField(default=123123)
    isVerified = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % self.user.username


class OptionData(models.Model):
    name = models.CharField(max_length=200)
    upto_3_mile = models.CharField(max_length=50, default=0, blank=True)
    from_3to5_mile = models.CharField(max_length=50, default=0, blank=True)
    from_5to10_mile = models.CharField(max_length=50, default=0, blank=True)
    from_10to30_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_upto_3_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_3to5_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_5to10_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_10to30_mile = models.CharField(max_length=50, default=0, blank=True)
    packing_charge = models.CharField(max_length=50, default=0, blank=True)
    supplier_fee = models.CharField(max_length=50, default=0, blank=True)
    vat = models.CharField(max_length=50, default=0, blank=True)
    cancel_charge = models.CharField(max_length=50, default=0, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.name


PAYMENT = (
    (1, 'Daily'),
    (2, 'Monthly'),
    (3, 'Weekly')
)


class VendorData(models.Model):
    objects = LocationManager()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    option = models.ForeignKey(OptionData, on_delete=models.CASCADE, null=True, blank=True)
    payment = models.PositiveIntegerField(choices=PAYMENT, default=3)
    shop_name = models.CharField(max_length=250, blank=True)
    email = models.EmailField(max_length=250, blank=True)
    mobile = models.CharField(max_length=50, blank=True)
    mobile2 = models.CharField(max_length=50, blank=True)
    latitude = models.CharField(max_length=100, null=True)
    longitude = models.CharField(max_length=100, null=True)
    rating = models.PositiveIntegerField(default=5)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    city = models.CharField(max_length=250, null=True)
    pincode = models.CharField(max_length=50, null=True)
    address = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    meat4all_per = models.CharField(max_length=100, null=True)
    logo = models.ImageField(upload_to='', null=True, blank=True)
    bank_name = models.CharField(max_length=200, null=True, blank=True)
    sort_code = models.CharField(max_length=200, null=True, blank=True)
    account_no = models.CharField(max_length=200, null=True, blank=True)
    holder_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    balance = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "%s" % self.shop_name


class VendorCertificateData(models.Model):
    vendor = models.ForeignKey(VendorData, on_delete=models.CASCADE, null=True, blank=True)
    file = models.FileField(upload_to='', null=True, blank=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.vendor


class VendorImageData(models.Model):
    vendor = models.ForeignKey(VendorData, on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to='', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.vendor


class ExecutiveData(models.Model):
    MODE = ((1, 'Motor Bike'),
              (2, 'Car'),
              (3, 'Bicycle'),
              )

    objects = LocationManager()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    mode = models.PositiveIntegerField(choices=MODE, default=1)
    payment = models.PositiveIntegerField(choices=PAYMENT, default=3)
    mobile = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=250, blank=True)
    mobile2 = models.CharField(max_length=50, blank=True)
    latitude = models.CharField(max_length=100, null=True)
    longitude = models.CharField(max_length=100, null=True)
    rating = models.PositiveIntegerField(default=5)
    city = models.CharField(max_length=250, null=True)
    de_charge_per = models.CharField(max_length=10, null=True)
    pincode = models.CharField(max_length=50, null=True)
    address = models.TextField(blank=True, null=True)
    v_name = models.CharField(max_length=100, null=True, blank=True)
    rc = models.FileField(upload_to='', null=True, blank=True)
    bank_name = models.CharField(max_length=200, null=True, blank=True)
    sort_code = models.CharField(max_length=200, null=True, blank=True)
    account_no = models.CharField(max_length=200, null=True, blank=True)
    holder_name = models.CharField(max_length=200, null=True, blank=True)

    dl_no = models.CharField(max_length=100, null=True, blank=True)
    dl_upload = models.FileField(upload_to='', null=True, blank=True)
    dl_date = models.DateField(null=True, blank=True)

    insurance = models.CharField(max_length=100, null=True, blank=True)
    insurance_upload = models.FileField(upload_to='', null=True, blank=True)

    is_verified = models.BooleanField(default=False)
    is_available = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    balance = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "%s" % self.user


class ChargeSettingData(models.Model):
    # delivery_charge = models.CharField(max_length=50, default=0, blank=True)
    upto_3_mile = models.CharField(max_length=50, default=0, blank=True)
    from_3to5_mile = models.CharField(max_length=50, default=0, blank=True)
    from_5to10_mile = models.CharField(max_length=50, default=0, blank=True)
    from_10to30_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_upto_3_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_3to5_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_5to10_mile = models.CharField(max_length=50, default=0, blank=True)
    driver_charge_from_10to30_mile = models.CharField(max_length=50, default=0, blank=True)
    packing_charge_per = models.CharField(max_length=50, default=0, blank=True)
    # packing_charge_difference = models.CharField(max_length=50, default=0, blank=True)
    distance = models.CharField(max_length=50, default=0, blank=True)
    tip_charge = models.CharField(max_length=50, default=0, blank=True)
    cancel_charge = models.CharField(max_length=50, default=0, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.upto_3_mile


class BannerData(models.Model):
    title = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='', null=True, blank=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return "%s" % self.title

