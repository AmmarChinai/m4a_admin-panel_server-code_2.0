# Generated by Django 2.0 on 2020-06-07 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0008_auto_20200529_1147'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vendordata',
            name='name',
        ),
        migrations.AddField(
            model_name='vendordata',
            name='rating',
            field=models.PositiveIntegerField(default=5),
        ),
        migrations.AddField(
            model_name='vendordata',
            name='shop_name',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
