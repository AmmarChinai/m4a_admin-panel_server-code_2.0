# Generated by Django 2.0 on 2020-06-09 05:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0010_executivedata_rating'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendordata',
            name='end_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='vendordata',
            name='start_time',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
