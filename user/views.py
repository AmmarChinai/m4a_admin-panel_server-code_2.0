import csv
import json
import random

import jwt
import pytz
from django.contrib.auth import authenticate, login, logout
from django.db.models import Sum
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from geopy.geocoders import Nominatim
# Create your views here.
from django.core.validators import validate_integer, validate_email
from django.http.response import JsonResponse, HttpResponseRedirect, FileResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.views import APIView
import datetime
import stripe

# import cv2
# import numpy as np
import matplotlib.pyplot as plt
from django.core.files.storage import FileSystemStorage

import requests

stripe.api_key = "sk_test_51IKcHYBNAGTtTpMZEwQU0GeFuWrODBDCxrHsnA4CgdoByJI24yQtgPBh7OWXjXMBcWAJ3j5ND0Hi6kOtyYH7i67Y00X82j7Ax7"

import keys
from category.models import SubCategoryData, VendorSubCategoryData, VendorOfferData, SettingData, OrderData, \
    CategoryData
from category.serialize import VendorDetailSerializer, VendorSubCategorySerializer, VendorListSerializer, \
    ExecutiveDetailSerializer, VendorOfferSerializer, VendorCategorySerializer, OrderListSerializer, \
    OrderDetailsSerializer, AddressSerializer
from delivey_app import settings
from user.forms import OthersUserCreationForm, VendorCreationForm, VendorCertificateCreationForm, ExecutiveCreationForm, \
    OptionCreationForm, ChargeSettingsCreationForm
from user.models import OtpData, User, VendorImageData, VendorData, ExecutiveData, VendorCertificateData, OptionData, \
    AddressData, AdminUserData, ChargeSettingData, BannerData


def jwt_encode(mobile):
    encoded_token = jwt.encode({'mobile': mobile}, keys.USER_LOGIN_SECRET_KEY, algorithm='HS256').decode('utf-8')
    return encoded_token


def jwt_decode(token):
    decoded_token = jwt.decode(token, keys.USER_LOGIN_SECRET_KEY, algorithms=['HS256'])
    return decoded_token


def index(request):
    return render(request, 'index.html')


from pyfcm import FCMNotification

proxy_dict = {
    "http": "http://127.0.0.1",
    "https": "http://127.0.0.1",
}
# push_service = FCMNotification(api_key="<api-key>", proxy_dict=proxy_dict)

push_service = FCMNotification(api_key=settings.FCM_API_KEY)


# For local server
# push_service = FCMNotification(api_key=settings.FCM_API_KEY, proxy_dict=proxy_dict)


def notify_user(user, message, title, data={}):
    registration_id = user.gcm_token
    if registration_id is None:
        return False

    if settings.SEND_FCM is False:
        print('Sending FCM disabled from settings.')
        return False

    result = push_service.notify_single_device(
        registration_id=registration_id,
        message_title=title,
        message_body=message,
        data_message=data)
    return result


def get_indian_date_time(datetime):
    """

    :param datetime:
    :return: Indian date-time
    """
    if not datetime:
        return {
            'date': '',
            'time': '',
        }

    local_tz = pytz.timezone('Europe/London')
    # local_tz = pytz.timezone(zone)
    local_date = datetime.replace(tzinfo=pytz.utc).astimezone(local_tz).strftime('%d/%m/%y')
    local_time = datetime.replace(tzinfo=pytz.utc).astimezone(local_tz).strftime('%H:%M:%S')

    return {
        'date': local_date,
        'time': local_time,
    }


def send_email(to, subject, body):
    api_key = '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'
    url = "https://api.sendinblue.com/v3/smtp/email"

    payload = {"sender": {"email": "contactus@meats4all.co.uk"},
               "to": [
                   {
                       "email": to,
                       "name": "Meats4All"
                   }
               ],
               "subject": subject,
               "htmlContent": body}

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "api-key": api_key
    }

    return requests.request("POST", url, json=payload, headers=headers)


# def send_sms(request):
def send_sms(to, text):
    api_key = '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'

    url = "https://api.sendinblue.com/v3/transactionalSMS/sms"

    payload = {
        "type": "transactional",
        "sender": "Meats4All",
        "recipient": str(to),
        "content": text
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "api-key": api_key
    }

    return requests.request("POST", url, json=payload, headers=headers)


@csrf_exempt
def banner_data(request):
    if request.method == 'GET':
        banner_queryset = BannerData.objects.all().order_by('-created')
        banner_list = []
        for banner_obj in banner_queryset:
            base_image_url = request.scheme + '://' + request.get_host() + '/media/'
            image = base_image_url + str(banner_obj.image)
            data = {
                'title': str(banner_obj.title),
                'image': image
            }
            banner_list.append(data)
        return JsonResponse({'success': True, 'banner_list': banner_list})


@csrf_exempt
def otp_verify(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        otp = data['otp']
        mobile = data['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid Mobile Number'})
        otp_valid = user.otpdata.code == int(otp)
        if not otp_valid:
            return JsonResponse({'success': False, 'message': 'Invalid Otp, please try again'})
        user.is_active = True

        user.save()
        token = jwt_encode(mobile)
        user.is_user = True
        user.save()
        return JsonResponse({'success': True, 'message': 'Success! Login', 'token': token,
                             'user_id': user.id})


def generate_otp(user):
    code = int(random.randint(123456, 999999))
    otp_obj, valid = OtpData.objects.get_or_create(user=user)
    otp_obj.code = code
    otp_obj.save()
    user.save()
    return user.otpdata.code


@csrf_exempt
def resend_otp(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        mobile = data['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid Mobile Number'})
        code = int(random.randint(123456, 999999))
        otp_obj = OtpData.objects.get(user=user)
        encoded_token = jwt_encode(user.username)
        otp_obj.code = code
        otp_obj.save()

        message = 'Dear User,\nThe secure OTP for account ' \
                  'registration is ' + str(code) + '. This OTP is valid for 15 mins only. Please ' \
                                                   'do not share this OTP with anyone.\n- Meats4all Team'

        try:
            send_sms(mobile, message)
        except Exception as e:
            print(str(e))
        return JsonResponse({'success': True, 'message': 'Otp has been re-send to your registered number',
                             'otp': code, 'token': str(encoded_token)})


@csrf_exempt
def forgot_password(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        mobile = data['mobile']
        is_login = data['is_login']
        if is_login == 'vendor':
            user = User.objects.filter(username=mobile, is_vendor=True).first()
            if not user:
                return JsonResponse({'success': False, 'message': 'Invalid Mobile Number'})
        elif is_login == 'driver':
            user = User.objects.filter(username=mobile, is_executive=True).first()
            if not user:
                return JsonResponse({'success': False, 'message': 'Invalid Mobile Number'})
        else:
            return JsonResponse({'success': False, 'message': 'Invalid Mobile Number'})

        code = int(random.randint(123456, 999999))
        otp_obj = OtpData.objects.get(user=user)
        encoded_token = jwt_encode(user.username)
        otp_obj.code = code
        otp_obj.save()
        message = 'Dear User,\nThe secure OTP for account ' \
                  'registration is ' + str(code) + '. This OTP is valid for 15 mins only. Please ' \
                                                   'do not share this OTP with anyone.\n- Meats4all Team'
        try:
            send_sms(mobile, message)
        except Exception as e:
            print(str(e))
        return JsonResponse({'success': True, 'message': 'Otp has been re-send to your registered number',
                             'otp': code, 'token': str(encoded_token), 'is_executive': user.is_executive,
                             'is_vendor': user.is_vendor})


@csrf_exempt
def login_user(request):
    if request.method == 'POST':
        # obj = User.is_active.get()
        data = json.loads(request.body)
        login = data['mobile']
        gcm_token = data['gcm_token']
        # is_active = data['is_active']
        user = ''
        username = ''

        # Check mobile login
        # if obj==True:
        # user_active = User.is_active.get(used=user.id)
        # if user_active == True:
        user, bool = User.objects.get_or_create(username=login)
        print(bool)
        print(user)
        print(user.is_active)
        if not user.is_active:
            # message = 'Your are blocked....\n- Meats4all Team'
            # try:
            #     send_sms(login, message)
            # except Exception as e:
            #     print(str(e))
            return JsonResponse({'success': False, 'message': 'User is InActive'})
        otp = generate_otp(user)
        message = 'Dear User,\nThe secure OTP for account ' \
                  'registration is ' + str(otp) + '. This OTP is valid for 15 mins only. Please ' \
                                                  'do not share this OTP with anyone.\n- Meats4all Team'
        try:
            send_sms(login, message)
        except Exception as e:
            print(str(e))
        user.first_name = 'New User'
        # user.is_user = True
        # if gcm_token is not '':
        #     user.gcm_token = gcm_token
        # user.save()
        encoded_token = jwt_encode(user.username)
        return JsonResponse({'success': True, "is_verified": True, 'message': 'Login Successful',
                             'otp': otp,
                             'is_profile_completed': user.is_profile_completed,
                             'is_active': user.is_active,
                             'is_exist': bool,
                             'token': str(encoded_token),
                             'user_id': user.id})





@csrf_exempt
def login_with_password(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        login = data['mobile']
        password = data['password']
        gcm_token = data['gcm_token']

        # Check mobile login
        vendor_id = 0
        executive_id = 0
        user = authenticate(username=login, password=password)
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid login credential'})

        if not user.isMobileVerified:
            otp = generate_otp(user)
            message = 'Dear User,\nThe secure OTP for account ' \
                      'registration is ' + str(otp) + '. This OTP is valid for 15 mins only. Please ' \
                                                      'do not share this OTP with anyone.\n- Meats4all Team'
            # send_mail('Account Verification', str(message), 'daaanindia@gmail.com', [user.username])

        user.isMobileVerified = True
        if gcm_token != '':
            user.gcm_token = gcm_token
        user.save()
        try:
            vendor_obj = VendorData.objects.filter(user=user).first()
            vendor_id = vendor_obj.id
        except Exception as e:
            vendor_id = ''
            print(str(e))
        try:
            executive_obj = ExecutiveData.objects.filter(user=user).first()
            executive_id = executive_obj.id
        except Exception as e:
            print(str(e))
            executive_id = ''

        encoded_token = jwt_encode(user.username)
        return JsonResponse({'success': True, "is_verified": True, 'message': 'Login Successful',
                             'is_vendor': user.is_vendor,
                             'is_executive': user.is_executive,
                             'token': str(encoded_token),
                             'executive_id': executive_id,
                             'vendor_id': vendor_id,
                             'user_id': user.id})


@csrf_exempt
def reset_password(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        otp = data['otp']
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        new_password = data['new_password']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'This mobile number is not registered'})

        otp_valid = user.otpdata.code == int(otp)
        if not otp_valid:
            return JsonResponse({'success': False, 'message': 'Invalid Otp, please try again'})
        # send_mail('Account Verification', str(message), 'daaanindia@gmail.com', [user.username])
        user.set_password(new_password)
        user.save()
        return JsonResponse({'success': True, 'message': 'Login Successfull'})


#
# @csrf_exempt
# def reset_password(request):
#     if request.method == 'POST':
#         data = json.loads(request.body)
#         mobile = data['mobile']
#         new_password = data['new_password']
#         user = User.objects.filter(username=mobile).first()
#         if not user:
#             return JsonResponse({'success': False, 'message': 'This Mobile is not registered'})
#         user.set_password(new_password)
#         user.save()
#         return JsonResponse({'success': True, 'message': 'Password Changed Successfully'})


@csrf_exempt
def change_password(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        old_password = data['old_password']
        new_password = data['new_password']
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']

        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Mobile Number is not registered'})
        username = user.username
        user = authenticate(username=username, password=old_password)
        if not user:
            return JsonResponse({'success': False, 'message': 'Your old Password is incorrect'})
        user.set_password(new_password)
        user.save()
        return JsonResponse({'success': True, 'message': 'Password Changed Successfully'})


@csrf_exempt
def delete(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        user_id = data['user_id']
        device_type = data['device_type']
        status = request.POST.get('status')
        obj = User.objects.get(id=user_id)
        if status == 'true' or status != 'true':
            obj.is_active = False
            obj.username = 'deleted-' + str(obj.id)
            obj.first_name = 'deleted-' + str(obj.id)
            obj.last_name = 'deleted-' + str(obj.id)
            obj.email = 'deleted-' + str(obj.id)
            # obj.avatar = None
        obj.save()
        return JsonResponse({'success': True,
                             'user_id': user_id,
                             'device_type': device_type,
                             'message': 'User is deleted successfully'})


@csrf_exempt
def home(request):
    if request.method == 'POST':
        setting_list = []
        data = json.loads(request.body)
        if "user_id" in data:
            user_id = data['user_id']
        else:
            user_id = ""
        token = data['token']
        gcm_token = data['gcm_token']
        latitude = data['latitude']
        longitude = data['longitude']
        device_type = data['device_type']
        proximity = SettingData.objects.get(key='PROXIMITY')
        settings_queryset = SettingData.objects.all()
        for obj in settings_queryset:
            data = {
                'key': obj.key,
                'value': obj.value
            }
            setting_list.append(data)
        try:
            decoded_token = jwt_decode(token)
            mobile = decoded_token['mobile']

            user = User.objects.filter(username=mobile).first()

            if not user:
                return JsonResponse({'success': False, 'message': 'Invalid User'})
            user.gcm_token = gcm_token
            user.device_type = device_type
            user.save()
        except Exception as e:
            print(str(e))
        nearby_vendors = VendorData.objects.nearby(
            latitude=latitude,
            longitude=longitude,
            proximity=proximity.value
        ).filter(
            is_active=True
        )
        if user_id == '':
            if nearby_vendors.count() == 0:
                return JsonResponse({'success': True,
                                     'vendors': [],
                                     'offers': [],
                                     'is_active': True
                                     })
            else:
                serializer = VendorListSerializer(nearby_vendors, many=True)
                # vendor_offer = VendorOfferData.objects.filter(vendor=nearby_vendors)
                # serializer_offer = VendorOfferSerializer(vendor_offer, many=True)

            return JsonResponse({'success': True,
                                 'vendors': serializer.data,
                                 'setting_list': setting_list,
                                 'is_active': True
                                 # 'offers': serializer_offer.data,
                                 })
        else:
            obj = User.objects.get(id=user_id)
            print(obj.id)
            print(obj.is_active)
            if nearby_vendors.count() == 0:
                return JsonResponse({'success': True,
                                     'vendors': [],
                                     'offers': [],
                                     'is_active': obj.is_active
                                     })
            else:
                serializer = VendorListSerializer(nearby_vendors, many=True)
                # vendor_offer = VendorOfferData.objects.filter(vendor=nearby_vendors)
                # serializer_offer = VendorOfferSerializer(vendor_offer, many=True)

            return JsonResponse({'success': True,
                                 'vendors': serializer.data,
                                 'setting_list': setting_list,
                                 'is_active': obj.is_active
                                 # 'offers': serializer_offer.data,
                                 })


@csrf_exempt
def search_vendor(request):
    if request.method == 'POST':
        setting_list = []
        data = json.loads(request.body)
        token = data['token']
        latitude = data['latitude']
        longitude = data['longitude']
        text = data['text']
        decoded_token = jwt_decode(token)
        print(decoded_token)
        mobile = decoded_token['mobile']
        proximity = SettingData.objects.get(key='PROXIMITY')
        settings_queryset = SettingData.objects.all()
        for obj in settings_queryset:
            data = {
                'key': obj.key,
                'value': obj.value
            }
            setting_list.append(data)

        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        nearby_vendors = VendorData.objects.nearby(
            latitude=latitude,
            longitude=longitude,
            proximity=proximity.value
        ).filter(
            is_active=True
        )
        print(nearby_vendors, 'kkkkkkkkkkkkkkkkk')
        if nearby_vendors.count() == 0:
            return JsonResponse({'success': True,
                                 'vendors': [],
                                 'offers': []
                                 })
        else:

            print(nearby_vendors, 'nearby_vendors')
            sub_cat_data = VendorSubCategoryData.objects.filter(sub_category__name__iexact=text,
                                                                vendor__in=nearby_vendors)
            if sub_cat_data.count() == 0:
                cat_data = VendorSubCategoryData.objects.filter(sub_category__category__name__icontains=text,
                                                                vendor__in=nearby_vendors)

                serializer = VendorCategorySerializer(cat_data, many=True)
            else:
                serializer = VendorSubCategorySerializer(sub_cat_data, many=True)

        # print(serializer)
        return JsonResponse({'success': True,
                             'sub_category_data': serializer.data,
                             'setting_list': setting_list,
                             })


@login_required
def user_data(request):
    base_image_url = request.scheme + '://' + request.get_host()
    user_list = User.objects.filter(is_executive=False, is_vendor=False, is_staff=False)
    paginator = Paginator(user_list, 20)
    page = request.GET.get('page')
    try:
        user_list = paginator.page(page)
    except PageNotAnInteger:
        user_list = paginator.page(1)
    except EmptyPage:
        user_list = paginator.page(paginator.num_pages)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'user/user_list.html', {'user_list': user_list, 'tab': int(tab),
                                                   'base_image_url': base_image_url})


@login_required
def vendor_data(request):
    base_image_url = request.scheme + '://' + request.get_host()
    vendor_list = VendorData.objects.all()
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'vendor/vendor_list.html', {'vendor_list': vendor_list, 'tab': int(tab),
                                                       'base_image_url': base_image_url})


@login_required
def add_vendor(request):
    if request.method == 'POST':
        logo = request.FILES.get('logo')
        image_list = request.FILES.getlist('image')
        name_list = request.POST.getlist('name')
        file_list = request.FILES.getlist('file')
        start_list = request.POST.getlist('start')
        end_list = request.POST.getlist('end')
        email = request.POST.get('email')
        print(image_list)
        password = 'Meats4All@123'
        user_form = OthersUserCreationForm(request.POST)
        print(user_form.is_valid())
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(password)
            user.is_vendor = True
            user.save()
            vendor_form = VendorCreationForm(request.POST)
            vendor = vendor_form.save()
            vendor.user = user
            vendor.logo = logo
            vendor.save()

            geocoder = Nominatim(user_agent=settings.GOOGLE_API_KEY)
            reverse = geocoder.reverse((vendor.latitude, vendor.longitude))
            vendor.address = reverse
            vendor.save()

            for obj in image_list:
                VendorImageData.objects.create(image=obj, vendor=vendor)

            c = min([len(name_list), len(file_list), len(start_list), len(end_list)])
            for i in range(c):
                # create a form instance and populate it with data from the request:
                file_form = VendorCertificateCreationForm({'name': name_list[i], 'file': file_list[i],
                                                           'start': start_list[i], 'end': end_list[i],
                                                           'vendor': vendor.id})
                file_obj = file_form.save()
                file_obj.file = file_list[i]
                file_obj.save()
            return redirect('/user/vendor_list/')
    else:
        base_image_url = request.scheme + '://' + request.get_host()
        api_key = settings.GOOGLE_API_KEY
        template_list = OptionData.objects.all()
        tab = request.GET.get('tab')
        if tab == '' or tab is None:
            tab = 1
        return render(request, 'vendor/add_vendor.html', {'base_image_url': base_image_url,
                                                          'template_list': template_list,
                                                          'tab': int(tab),
                                                          'api_key': api_key})


@login_required
def view_vendor(request, vendor_id):
    base_image_url = request.scheme + '://' + request.get_host()
    vendor_obj = VendorData.objects.get(id=int(vendor_id))
    vendor_image_list = VendorImageData.objects.filter(vendor=vendor_obj)
    sub_category_list = VendorSubCategoryData.objects.filter(vendor=vendor_obj)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'vendor/profile.html', {'vendor_obj': vendor_obj,
                                                   'base_image_url': base_image_url,
                                                   'tab': int(tab),
                                                   'sub_category_list': sub_category_list,
                                                   'vendor_image_list': vendor_image_list})


@login_required
def view_executive(request, executive_id):
    base_image_url = request.scheme + '://' + request.get_host()
    executive_obj = ExecutiveData.objects.get(id=int(executive_id))
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'executive/profile.html', {'executive_obj': executive_obj,
                                                      'base_image_url': base_image_url,
                                                      'tab': int(tab)
                                                      })


def vendor_data_mobile(request):
    vendor_list = []
    vendors = VendorData.objects.filter()
    serializer = VendorListSerializer(vendors, many=True)
    return JsonResponse({
        "error": False,
        "vendor": serializer.data,
    })


@csrf_exempt
def vendor_offer_mobile(request, vendor_id):
    vendor = VendorData.objects.get(id=int(vendor_id))
    vendor_offer = VendorOfferData.objects.filter(vendor__id=int(vendor_id))
    if request.method == 'GET':
        serializer = VendorOfferSerializer(vendor_offer, many=True)
        return JsonResponse({
            "error": False,
            "offer": serializer.data,
        })
    else:
        data = json.loads(request.body)
        vendor_offer = VendorOfferData.objects.create(vendor=vendor)
        vendor_offer.name = data['name']
        vendor_offer.percentage = data['percentage']
        vendor_offer.start = data['start']
        vendor_offer.description = data['description']
        vendor_offer.end = data['end']
        code = data['name'][:3].upper() + data['percentage'][:2]
        vendor_offer.code = code
        vendor_offer.save()

        user_queryset = User.objects.all()
        message = 'Dear User, There is an offer from "' + str(vendor.shop_name) + '" with ' \
                  + str(vendor_offer.percentage) + '% discount ' \
                                                   'for all meat products ' + str(
            vendor_offer.name) + ' Please go ahead and place your order ' \
                                 'now with offer code “' + str(code) + '“ and redeem the offers.\n- Meats4all Team'

        for obj in user_queryset:
            try:
                send_sms(obj.username, message)
            except Exception as e:
                print(str(e))
        return JsonResponse({
            "error": False,
            "message": "Offer Updated"
        })


@csrf_exempt
def vendor_offer_delete(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        vendor_id = data['vendor_id']
        offer_id = data['offer_id']
        print(offer_id)
        print(vendor_id)

        try:
            vendor_offer_obj = VendorOfferData.objects.filter(id=int(offer_id))
            vendor_offer_obj.delete()
            return JsonResponse({'success': True,
                                 'message': 'Your Offer is deleted successfully'})

        except VendorOfferData.DoesNotExist:
            # print("offer is not available")
            return JsonResponse({'success': False,
                                 'message': 'Offer is Not available'})

    return JsonResponse({'success': False,
                         'message': 'Offer is Not available'})


@csrf_exempt
def vendor_orders(request, vendor_id):
    # vendor = VendorData.objects.get(id=int(vendor_id))
    try:
        token = request.GET.get('token')
        decoded_token = jwt_decode(token)
    except Exception as e:
        return JsonResponse({'success': False, 'message': 'Invalid User'})

    vendor_orders = OrderData.objects.filter(vendor__id=int(vendor_id))
    if request.method == 'GET':
        serializer = OrderListSerializer(vendor_orders, many=True)
        return JsonResponse({
            "error": False,
            "offer": serializer.data,
        })


@csrf_exempt
def pending_orders(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        proximity = SettingData.objects.get(key='EXECUTIVE_PROXIMITY')
        print(proximity.value, 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk')
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        latitude = data['latitude']
        longitude = data['longitude']

        orders = OrderData.objects.nearby(
            latitude=latitude,
            longitude=longitude,
            proximity=proximity.value
        ).filter(status=8, assigned_executive=False)
        serializer = OrderListSerializer(orders, many=True)
        return JsonResponse({
            "error": False,
            "offer": serializer.data,
        })


@csrf_exempt
def executive_orders(request, executive_id):
    # vendor = VendorData.objects.get(id=int(vendor_id))
    try:
        token = request.GET.get('token')
        decoded_token = jwt_decode(token)
    except Exception as e:
        return JsonResponse({'success': False, 'message': 'Invalid User'})

    executive_orders = OrderData.objects.filter(delivery_boy__id=int(executive_id))
    if request.method == 'GET':
        serializer = OrderListSerializer(executive_orders, many=True)
        return JsonResponse({
            "error": False,
            "offer": serializer.data,
        })


def vendor_details(request, vendor_id):
    vendor = VendorData.objects.get(id=int(vendor_id))
    vendor_image_list = []
    vendor_certificates_list = []
    serializer = VendorDetailSerializer(vendor)
    # sub_category = VendorSubCategorySerializer(vendor.id)
    vendor_image_queryset = VendorImageData.objects.filter(vendor=vendor)
    base_image_url = request.scheme + '://' + request.get_host()
    try:
        for obj in vendor_image_queryset:
            image = base_image_url + '/media/' + str(obj.image)
            vendor_image_list.append(image)
    except Exception as e:
        print(str(e))
    try:
        vendor_certificates_queryset = VendorCertificateData.objects.filter(vendor=vendor)
        for obj in vendor_certificates_queryset:
            data = {}
            data['name'] = obj.name
            data['start'] = str(obj.start)
            data['end'] = str(obj.end)
            data['file'] = base_image_url + '/media/' + str(obj.file)
            vendor_certificates_list.append(data)
    except Exception as e:
        print(str(e))

    vendor_rating = static_vendor_rating(vendor_id)
    return JsonResponse({
        "error": False,
        "vendor": serializer.data,
        # "sub_category_data": sub_category.data,
        'vendor_image_list': vendor_image_list,
        'vendor_certificates': vendor_certificates_list,
        'base_image_url': base_image_url,
        'vendor_rating': vendor_rating
    })


def executive_details(request, executive_id):
    executive = ExecutiveData.objects.get(id=int(executive_id))
    serializer = ExecutiveDetailSerializer(executive)
    base_image_url = request.scheme + '://' + request.get_host()
    # sub_category = VendorSubCategorySerializer(vendor.id)
    executive_rating = static_executive_rating(executive_id)
    return JsonResponse({
        "error": False,
        "executives": serializer.data,
        # "sub_category_data": sub_category.data,
        'base_image_url': base_image_url,
        'executive_rating': executive_rating
    })


@csrf_exempt
def add_subcategory_vendor(request, vendor_id):
    if request.method == 'POST':
        # decoded_token = jwt_decode(token)
        # mobile = decoded_token['mobile']
        # user = User.objects.filter(username=mobile).first()
        # if not user:
        #     return JsonResponse({'success': False, 'message': 'Invalid User'})
        vendor_obj = VendorData.objects.get(id=int(vendor_id))
        # for obj in sub_category_list:
        data = json.loads(request.body)
        sub_category_data = data['sub_category_data']
        category_id = data['category_id']
        queryset = VendorSubCategoryData.objects.filter(sub_category__category__id=int(category_id), vendor=vendor_obj)
        for obj in queryset:
            obj.is_available = False
            obj.save()
        for item in sub_category_data:
            sub_category_obj = SubCategoryData.objects.filter(id=item['id']).first()
            print(vendor_obj, 'aaaaaaaaaaaaaaaaaaaaaaaa')
            obj, bool = VendorSubCategoryData.objects.get_or_create(sub_category=sub_category_obj, vendor=vendor_obj)
            obj.price = item['price']
            obj.quantity = item['quantity']
            obj.offer_price = item['offer_price']
            obj.unit = item['unit']
            obj.is_available = True
            obj.save()
    return JsonResponse({'success': True, 'message': 'Sub Category Updated'})


@login_required
def edit_vendor(request, vendor_id):
    vendor_obj = VendorData.objects.get(id=int(vendor_id))
    if request.method == 'POST':
        logo = request.FILES.get('logo')
        image_list = request.FILES.getlist('image')
        name_list = request.POST.getlist('name')
        status = request.POST.get('status')
        first_name = request.POST.get('first_name')
        file_list = request.FILES.getlist('file')

        image_list1 = request.FILES.getlist('image1')
        image_id1 = request.FILES.getlist('image_id1')
        name_list1 = request.POST.getlist('name1')
        file_list1 = request.FILES.getlist('file1')

        start_list1 = request.POST.getlist('start1')
        end_list1 = request.POST.getlist('end1')
        cretificate_id1 = request.POST.getlist('cretificate_id1')

        start_list = request.POST.getlist('start')
        end_list = request.POST.getlist('end')
        email = request.POST.get('email')

        vendor_form = VendorCreationForm(request.POST, instance=vendor_obj)
        print(vendor_form)
        # vendor = vendor_form.save()
        # print(vendor_form)
        geocoder = Nominatim(user_agent=settings.GOOGLE_API_KEY)
        reverse = geocoder.reverse((vendor_obj.latitude, vendor_obj.longitude))
        vendor_obj.address = reverse
        vendor_obj.email = email
        if logo is not None:
            vendor_obj.logo = logo
        vendor_obj.is_active = status
        vendor_obj.user.first_name = first_name
        vendor_obj.user.save()
        vendor_obj.save()

        for obj in image_id1:
            image_obj = VendorImageData.objects.get(id=int(obj.id))
            image_obj.image = image_list1
            image_list1.pop(0)
            image_obj.save()

        for obj in image_list:
            VendorImageData.objects.create(image=obj, vendor=vendor_obj)

        d = min([len(name_list1), len(file_list1), len(start_list1), len(end_list1), len(cretificate_id1)])
        for i in range(d):
            # create a form instance and populate it with data from the request:
            file_instance = VendorCertificateData.objects.get(id=int(cretificate_id1[i]))
            file_instance.file = file_list1[i]
            file_instance.name = name_list1[i]
            file_instance.start = start_list1[i]
            file_instance.end = end_list1[i]
            file_instance.save()
        try:
            c = min([len(name_list), len(file_list), len(start_list), len(end_list)])
            for i in range(c):
                # create a form instance and populate it with data from the request:
                file_form = VendorCertificateCreationForm({'name': name_list[i], 'file': file_list[i],
                                                           'start': start_list[i], 'end': end_list[i],
                                                           'vendor': vendor_obj.id})

                file_obj = file_form.save()
                file_obj.file = file_list[i]
                file_obj.save()
        except Exception as e:
            print(str(e))
        return redirect('/user/vendor_list/')
    else:
        base_image_url = request.scheme + '://' + request.get_host()
        vendor_obj = VendorData.objects.get(id=int(vendor_id))
        img_list = VendorImageData.objects.filter(vendor=vendor_obj)
        img_count = VendorImageData.objects.filter(vendor=vendor_obj).count()
        image_req = int(5 - img_count)
        certificate_list = VendorCertificateData.objects.filter(vendor=vendor_obj)
        template_list = OptionData.objects.all()
        api_key = settings.GOOGLE_API_KEY
        tab = request.GET.get('tab')
        if tab == '' or tab is None:
            tab = 1
        return render(request, 'vendor/edit_vendor.html', {'vendor_obj': vendor_obj,
                                                           'img_list': img_list,
                                                           'template_list': template_list,
                                                           'image_req': range(image_req),
                                                           'api_key': api_key,
                                                           'tab': int(tab),
                                                           'certificate_list': certificate_list,
                                                           'base_image_url': base_image_url})


@login_required
def executive_data(request):
    base_image_url = request.scheme + '://' + request.get_host()
    executive_list = ExecutiveData.objects.all()
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'executive/executive_list.html', {'executive_list': executive_list,
                                                             'tab': int(tab),
                                                             'base_image_url': base_image_url})


@login_required
def option_data(request):
    if request.method == 'POST':
        if 'add' in request.POST:
            option_form = OptionCreationForm(request.POST)
            option = option_form.save()
            option.save()
        elif 'edit' in request.POST:
            edit_option_id = request.POST.get('edit_option_id')
            edit_option = OptionData.objects.get(id=int(edit_option_id))
            option_form = OptionCreationForm(request.POST, instance=edit_option)
            option = option_form.save()
            option.save()
        #     name = request.POST.get('name')
        #     image = request.FILES.get('image')
        #     category_id = request.POST.get('category_id')
        #     obj = CategoryData.objects.get(id=int(category_id))
        #     if image is not None:
        #         obj.image = image
        #     obj.name = name
        #     obj.save()
        return redirect('/user/option_list/')
    else:
        base_image_url = request.scheme + '://' + request.get_host()
        option_list = OptionData.objects.all()
        return render(request, 'option_list.html', {'option_list': option_list,
                                                    'base_image_url': base_image_url})


@login_required
def add_executive(request):
    if request.method == 'POST':
        avatar = request.FILES.get('avatar')
        rc = request.FILES.get('rc')
        dl_upload = request.FILES.get('dl_upload')
        insurance_upload = request.FILES.get('insurance_upload')
        user_form = OthersUserCreationForm(request.POST)
        print(user_form.is_valid())

        password = 'Meats4All@123'
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(password)
            user.is_executive = True
            user.avatar = avatar
            user.save()
            executive_form = ExecutiveCreationForm(request.POST)
            executive = executive_form.save()
            executive.user = user
            executive.save()

            geocoder = Nominatim(user_agent=settings.GOOGLE_API_KEY)
            reverse = geocoder.reverse((executive.latitude, executive.longitude))
            executive.address = reverse
            executive.rc = rc
            executive.insurance_upload = insurance_upload
            executive.dl_upload = dl_upload
            executive.save()

            return redirect('/user/executive_list/')
        else:
            pass
    else:
        base_image_url = request.scheme + '://' + request.get_host()
        api_key = settings.GOOGLE_API_KEY
        tab = request.GET.get('tab')
        if tab == '' or tab is None:
            tab = 1
        return render(request, 'executive/add_executive.html', {'base_image_url': base_image_url,
                                                                'tab': int(tab),
                                                                'api_key': api_key})


def edit_executive(request, executive_id):
    executive_obj = ExecutiveData.objects.get(id=int(executive_id))
    if request.method == 'POST':
        avatar = request.FILES.get('avatar')
        name = request.POST.get('first_name')
        status = request.POST.get('status')
        rc = request.FILES.get('rc')
        dl_upload = request.FILES.get('dl_upload')
        insurance_upload = request.FILES.get('insurance_upload')
        if avatar is not None:
            executive_obj.user.avatar = avatar
        if dl_upload is not None:
            executive_obj.dl_upload = dl_upload
        if insurance_upload is not None:
            executive_obj.insurance_upload = insurance_upload
        executive_obj.user.first_name = name
        executive_obj.user.save()
        executive_form = ExecutiveCreationForm(request.POST, instance=executive_obj)
        executive = executive_form.save()
        executive.save()

        geocoder = Nominatim(user_agent=settings.GOOGLE_API_KEY)
        reverse = geocoder.reverse((executive.latitude, executive.longitude))
        executive.address = reverse
        executive_obj.is_verified = status
        if rc is not None:
            executive.rc = rc
        executive.save()

        return redirect('/user/executive_list/')
    else:
        base_image_url = request.scheme + '://' + request.get_host()
        api_key = settings.GOOGLE_API_KEY
        tab = request.GET.get('tab')
        if tab == '' or tab is None:
            tab = 1
        return render(request, 'executive/edit_executive.html', {'base_image_url': base_image_url,
                                                                 'executive_obj': executive_obj,
                                                                 'tab': int(tab),
                                                                 'api_key': api_key})


@csrf_exempt
def user_profile(request):
    if request.method == 'GET':
        user_id = request.GET.get('user_id')
        user = User.objects.get(id=int(user_id))
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        user_json = {}
        user_json['user_id'] = user.id
        user_json['user_first_name'] = user.first_name
        user_json['user_last_name'] = user.last_name
        user_json['user_gender'] = user.gender
        user_json['user_username'] = user.username
        user_json['user_email'] = user.email
        user_json['is_profile_completed'] = user.is_profile_completed
        user_json['user_avatar'] = request.scheme + '://' + request.get_host() + '/media/' + str(user.avatar)
        return JsonResponse({'success': True, 'user_json': user_json})
    elif request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']
        gender = data['gender']
        if not token:
            return JsonResponse({'success': False, 'message': 'Invalid Token', })
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        user.first_name = first_name
        user.last_name = last_name
        user.gender = gender
        user.email = email
        user.is_profile_completed = True
        user.save()
        # message_for_mail = 'Hello! Thank you for your interest in Meats4all. Please find below the ' \
        #                    'links that will help you download the app'
        #
        # signature = 'We look forward to serving you the best meats from your ' \
        #             'local meat vendors. Best Regards, - Meats4all Team'
        if user.is_email:
            print("is_email==if>", user.is_email)
            user.is_email = False
            user.save()
            html_content = render_to_string('Welcome_Email_template.html',
                                            {
                                                # 'text': message_for_mail,
                                                # 'link': 'http://onelink.to/zma8ze',
                                                # 'order_obj': order_obj,
                                                # 'bottom_text': 'Download',
                                                # 'signature': signature
                                            })

            send_email(to=email, subject="Your Meats4all Registration is One Step Away!", body=html_content)
            return JsonResponse({
                'success': True,
                'message': 'Profile Updated Successfully',
                "error": False
            })
        else:
            print("is_email==else>", user.is_email)
            return JsonResponse({
                'success': True,
                'message': 'Profile Updated Successfully',
                "error": False
            })


@csrf_exempt
def edit_avatar(request):
    if request.method == 'POST':
        avatar = request.FILES.get('avatar')
        token = request.POST.get('token')
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        user.avatar = avatar
        user.save()
        base_image_url = request.scheme + '://' + request.get_host()
        avatar = base_image_url + '/media/' + str(avatar)
        return JsonResponse({'success': True, 'message': 'Image Updated successfully', 'avatar': str(avatar)})


@csrf_exempt
def change_mobile_email(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        otp = data['otp']
        new_email = data['email']
        mobile = data['mobile']
        is_mobile_changed = data['is_mobile_changed']
        is_email_changed = data['is_email_changed']
        if not token:
            return JsonResponse({'success': False, 'message': 'Invalid Token'})
        decoded_token = jwt_decode(token)
        email = decoded_token['email']
        user = User.objects.filter(username=email).first()
        current_user_email = user.username
        current_user_mobile = user.mobile

        # if current_user_email.lower() != new_email.lower():
        if is_email_changed:
            Is_email_Exist = User.objects.filter(username=new_email).exists()
            if Is_email_Exist:
                return JsonResponse({'success': False, 'message': 'This Email Id is already exist'})
            else:
                pass
        # if current_user_email != current_user_mobile:
        if is_mobile_changed:
            Is_mobile_Exist = User.objects.filter(mobile=mobile).exists()
            if Is_mobile_Exist:
                return JsonResponse({'success': False, 'message': 'This Mobile Number is already exist'})

            else:
                pass
        otp_valid = user.otpdata.code == int(otp)
        if not otp_valid:
            return JsonResponse({'success': False, 'message': 'Invalid Otp, please try again'})
        user.username = new_email
        user.mobile = mobile
        user.save()
        return JsonResponse({'success': True, 'message': 'Profile Updated Successfully'})


# link = 'https://twilix.exotel.in/v1/Accounts/' + str(EXOTEL_SID) + '/Sms/send.json'


# Send OTP
def send_otp(mobile, type):
    # Get user
    try:
        user = User.objects.get(mobile=mobile)
    except User.DoesNotExist:
        return None

    username = user.first_name or 'User'
    otp = generateOtp(user)

    smsText = getSMSText(username, otp, type)
    print(smsText)

    # if SEND_SMS != "True":
    #     return False

    # return postSMS(mobile, smsText)


# Generate SMS text
def getSMSText(name, otp, type):
    if type == 'welcome':
        return 'Hello ' + str(name) + \
               ', Thank you for Registering with iRelief services. iRelief Team.'
    elif type == 'recover':
        return 'OTP to Reset Password for iRelief Services is ' + str(otp) \
               + '. Please enter within 30 minutes. iRelief Team.'
    elif type == 'activation':
        return 'Registration OTP for iRelief Service is ' + str(
            otp) + '. Please enter within 30 minutes. iRelief Team.'
    elif type == 'deactivate':
        return 'OTP to deactivate your account for iRelief Service is ' + str(
            otp) + '. Please enter within 30 minutes. iRelief Team.'
    elif type == 'activate':
        return 'OTP to activate your account for iRelief Service is ' + str(
            otp) + '. Please enter within 30 minutes. iRelief Team.'
    return 'Request invalid'


# Generates otp for user
def generateOtp(user):
    user.otpdata.code = int(random.randint(123456, 999999))
    user.otpdata.sentat = datetime.datetime.now()
    user.save()
    return user.otpdata.code


# # Sends SMS to user
# def postSMS(mobile, text):
#     res = requests.post(link,
#                         auth=(EXOTEL_SID, EXOTEL_TOKEN),
#                         data={
#                             'From': 'VK-iRelief',
#                             'To': mobile,
#                             'Body': text
#                         })
#     print(res.json())
#


@csrf_exempt
def login_admin(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            token = jwt.encode({'username': request.user.username}, keys.USER_LOGIN_SECRET_KEY,
                               algorithm='HS256').decode('utf-8')
            return HttpResponseRedirect('/home/')
        else:
            return render(request, 'login.html')

    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is None:
            return render(request, 'login.html', {'success': 1})
        if not user.isMobileVerified:
            return render(request, 'login.html', {'success': 1})
        if user.is_staff:
            token = jwt.encode({'username': username}, keys.USER_LOGIN_SECRET_KEY, algorithm='HS256')
            login(request, user)
            return HttpResponseRedirect('/home/')


def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')


def getStripeData():
    try:
        stripeBalance = stripe.Balance.retrieve()
        balance = str(stripeBalance.available[0]['amount'])
        print(float(balance) / 100)
        currentBalance = float(balance) / 100
        return currentBalance
    except:

        return "Unknown"


@login_required
def dashboard(request):
    total_category = CategoryData.objects.all().count()
    total_sub_category = SubCategoryData.objects.all().count()
    total_vendor = VendorData.objects.all().count()
    total_driver = ExecutiveData.objects.all().count()
    total_user = User.objects.filter(is_superuser=False, is_vendor=False, is_executive=False, is_staff=False).count()
    in_progress_orders = OrderData.objects.filter().exclude(status__in=[1, 6]).count()
    complete_orders = OrderData.objects.filter(status=6).count()
    pending_orders = OrderData.objects.filter(status=1).count()
    total_orders = OrderData.objects.all().count()
    stripe_balance = getStripeData()
    try:
        total_payout_due = VendorData.objects.aggregate(Sum('balance'))
        total_payout_due = total_payout_due['balance__sum']
        total_payout_due = round(total_payout_due, 2)
    except:
        total_payout_due = 0
    try:
        total_driver_payout_due = ExecutiveData.objects.aggregate(Sum('balance'))
        total_driver_payout_due = total_driver_payout_due['balance__sum']
        total_driver_payout_due = round(total_driver_payout_due, 2)
    except:
        total_payout_due = 0
    try:
        total_earning = OrderData.objects.filter(status=6, payment_status=2).aggregate(Sum('total_bill'))
        total_earning = total_earning['total_bill__sum']
    except Exception as e:
        total_earning = 0
    # try:
    monthly_earning = OrderData.objects.filter(status=6, payment_status=2,
                                               created__month=datetime.datetime.today().date().month).aggregate(
        Sum('total_bill'))
    print(datetime.datetime.today().month, 'vvvvvvvvvvvvvvvvvvvvvvvvv')
    print(monthly_earning, 'monthly_earningmonthly_earning')
    monthly_earning = monthly_earning['total_bill__sum']
    # except Exception as e:
    #     monthly_earning = 0
    try:
        annual_earning = OrderData.objects.filter(status=6, payment_status=2,
                                                  created__year=datetime.datetime.today().date().year).aggregate(
            Sum('total_bill'))
        annual_earning = annual_earning['total_bill__sum']
    except Exception as e:
        annual_earning = 0

    # try:
    #     pending_payment = OrderData.objects.filter(status=6, payment_status=2).aggregate(Sum('total_bill'))
    #     total_earning = total_earning['total_bill__sum']
    # except Exception as e:
    #     total_earning = 0

    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'base.html', {'total_category': total_category,
                                         'tab': int(tab),
                                         'total_sub_category': total_sub_category,
                                         'total_vendor': total_vendor,
                                         'total_driver': total_driver,
                                         'total_orders': total_orders,
                                         'in_progress_orders': in_progress_orders,
                                         'complete_orders': complete_orders,
                                         'pending_orders': pending_orders,
                                         'total_earning': 0 if total_earning is None else total_earning,
                                         'monthly_earning': 0 if monthly_earning is None else monthly_earning,
                                         'annual_earning': 0 if annual_earning is None else annual_earning,
                                         'total_user': total_user,
                                         'stripe_balance': stripe_balance,
                                         'total_payout_due': 0 if total_payout_due is None else total_payout_due,
                                         'total_driver_payout_due': 0 if total_driver_payout_due is None else total_driver_payout_due

                                         })


@login_required
def sample(request):
    return render(request, 'base.html')


def tnc(request):
    path = '/var/www/delivery_app/templates/pdf/new_tnc.pdf'
    return FileResponse(open(path, 'rb'), content_type='application/pdf')


def driver_tnc(request):
    path = '/var/www/delivery_app/templates/pdf/executive_tnc.pdf'
    return FileResponse(open(path, 'rb'), content_type='application/pdf')


def partner_tnc(request):
    path = '/var/www//delivery_app/templates/pdf/new_vendor_tnc.pdf'
    return FileResponse(open(path, 'rb'), content_type='application/pdf')


def privacy_policy(request):
    path = '/var/www/delivery_app/templates/pdf/new_pp.pdf'
    return FileResponse(open(path, 'rb'), content_type='application/pdf')


def help_view(request):
    path = '/var/www/delivery_app/templates/pdf/help.pdf'
    return FileResponse(open(path, 'rb'), content_type='application/pdf')


@login_required
def settings_data(request):
    obj = ChargeSettingData.objects.first()
    if request.method == 'POST':
        settings_form = ChargeSettingsCreationForm(request.POST, instance=obj)
        print(settings_form.is_valid())
        print(settings_form)
        settings_form.save()
        return redirect('/settings/')
    else:

        tab = request.GET.get('tab')
        if tab == '' or tab is None:
            tab = 1
            print(obj, 'llllllllllllllllllllllllll')
        return render(request, 'settings.html', {'obj': obj, 'tab': int(tab)})


def order_data(request):
    token = request.GET.get('token')
    decoded_token = jwt_decode(token)
    mobile = decoded_token['mobile']
    user = User.objects.filter(username=mobile).first()
    if not user:
        return JsonResponse({'success': False, 'message': 'Invalid User'})
    order_list = OrderData.objects.filter(user=user).order_by('-created')
    serializer = OrderListSerializer(order_list, many=True)
    base_image_url = request.scheme + '://' + request.get_host()
    # sub_category = VendorSubCategorySerializer(vendor.id)

    return JsonResponse({
        "error": False,
        "orders": serializer.data,
        'base_image_url': base_image_url,
    })


@csrf_exempt
def become_partner(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        type = request.POST.get('type')
        company_name = request.POST.get('company_name')
        mobile = request.POST.get('mobile')
        email = request.POST.get('email')
        body = "<html><head></head><body><p>Hello,</p><p>Name: " + str(name) + "</p><p>Type: " + str(
            type) + "</p><p>Company_name: " + str(company_name) + "</p><p>Mobile: " + str(
            mobile) + "</p><p>Email: " + str(email) + "</p></body></html>"

        send_email(to='contactus@meats4all.co.uk', subject="Become a Partner", body=body)
        return JsonResponse({
            "error": False,
            "message": "Your Query has been Sent."
        })


@csrf_exempt
def subscribe(request):
    if request.method == 'GET':
        email = request.GET.get('email')
        message_for_mail = 'Hello! Thank you for your interest in Meats4all. Please find below the ' \
                           'links that will help you download the app'

        signature = 'We look forward to serving you the best meats from your ' \
                    'local meat vendors. Best Regards, - Meats4all Team'

        html_content = render_to_string('email_template.html',
                                        {'text': message_for_mail,
                                         'link': 'http://onelink.to/zma8ze',
                                         # 'order_obj': order_obj,
                                         'bottom_text': 'Download',
                                         'signature': signature
                                         })

        send_email(to=email, subject="Your Meats4all Registration is One Step Away!", body=html_content)
        return JsonResponse({
            "error": False,
            "message": "You have successfully subscribed."
        })


def order_details_mobile(request, order_id):
    token = request.GET.get('token')
    # decoded_token = jwt_decode(token)
    # mobile = decoded_token['mobile']
    # user = User.objects.filter(username=mobile).first()
    # if not user:
    #     return JsonResponse({'success': False, 'message': 'Invalid User'})

    order_obj = OrderData.objects.get(id=int(order_id))
    serializer = OrderDetailsSerializer(order_obj)
    base_image_url = request.scheme + '://' + request.get_host()
    # sub_category = VendorSubCategorySerializer(vendor.id)

    return JsonResponse({
        "error": False,
        "orders": serializer.data,
        'base_image_url': base_image_url,
    })


@csrf_exempt
def cancel_order(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            reason = data['reason']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.status = 7
        order_obj.cancel_reason = reason
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'cancel'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.vendor.shop_name) + ", " \
                                                                      "Order with order ID: " + str(
                order_obj.custom_id) + str(
                order_obj.created) + ' . has been cancelled by customer. Reason: ' + str(reason) + ' .'
            notify_user(
                user=order_obj.vendor.user,
                message=notify_text,
                title='Order Cancelled',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Cancelled',
        })


@csrf_exempt
def address_data(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        address_id = data['address_id']
        token = data['token']
        block = data['block']
        title = data['title']
        address = data['address']
        latitude = data['latitude']
        longitude = data['longitude']
        landmark = data['landmark']

        if int(address_id) == 0:
            try:
                decoded_token = jwt_decode(token)
                mobile = decoded_token['mobile']
                user = User.objects.filter(username=mobile).first()
                address_obj = AddressData.objects.create(user=user, block=block, title=title, address=address,
                                                         landmark=landmark,
                                                         latitude=latitude, longitude=longitude)

            except Exception as e:
                return JsonResponse({'success': False, 'message': 'Invalid User ' + str(e)})
        else:
            decoded_token = jwt_decode(token)
            mobile = decoded_token['mobile']
            user = User.objects.filter(username=mobile).first()
            address_obj = AddressData.objects.get(id=int(address_id))
            address_obj.block = block
            address_obj.title = title
            address_obj.landmark = landmark
            address_obj.address = address
            address_obj.latitude = latitude
            address_obj.longitude = longitude
            address_obj.save()

        return JsonResponse({
            "error": False,
            "message": 'Address Updated',
            "address_id": address_obj.id
        })

    elif request.method == 'DELETE':
        data = json.loads(request.body)
        token = data['token']
        address_id = data['address_id']
        address_obj = AddressData.objects.get(id=int(address_id))
        address_obj.delete()

        return JsonResponse({
            "error": False,
            "message": 'Address Deleted',
        })

    else:
        token = request.GET.get('token')
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        address_list = AddressData.objects.filter(user=user).order_by('-created')
        serializer = AddressSerializer(address_list, many=True)
        base_image_url = request.scheme + '://' + request.get_host()
        # sub_category = VendorSubCategorySerializer(vendor.id)

        return JsonResponse({
            "error": False,
            "address": serializer.data,
            'base_image_url': base_image_url,
        })


@csrf_exempt
def executive_status(request, executive_id):
    """Api to update the status of executives"""

    if request.method == 'GET':
        """
        Returns the status of executive who logged in to device
        """
        token = request.GET.get('token')
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        executive_obj = ExecutiveData.objects.get(id=int(executive_id))

        return JsonResponse({
            'error': False,
            'status': executive_obj.is_available
        })

    else:
        data = json.loads(request.body)
        token = data['token']
        status = data['status']
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        executive_obj = ExecutiveData.objects.get(id=int(executive_id))
        executive_obj.is_available = status
        executive_obj.save()
        return JsonResponse({
            'error': False,
            'status': executive_obj.is_available,
        })


@login_required
def admin_user_list(request):
    if request.method == 'POST':
        if 'add' in request.POST:
            first_name = request.POST.get('first_name')
            username = request.POST.get('username')
            category = request.POST.get('category')
            sub_category = request.POST.get('sub_category')
            vendor = request.POST.get('vendor')
            driver = request.POST.get('driver')
            option = request.POST.get('option')
            order = request.POST.get('order')
            admin_access = request.POST.get('admin_access')
            password = request.POST.get('password')
            print(category, 'category')
            print(sub_category, 'sub_category')
            obj, bool = User.objects.get_or_create(username=username)
            obj.first_name = first_name
            obj.set_password(password)
            obj.is_staff = True
            obj.save()
            admin_user_obj, bool = AdminUserData.objects.get_or_create(user=obj)
            if category == '':
                admin_user_obj.is_category_access = True
            else:
                admin_user_obj.is_category_access = False

            if sub_category == '':
                admin_user_obj.is_sub_category_access = True
            else:
                admin_user_obj.is_sub_category_access = False

            if vendor == '':
                admin_user_obj.is_vendor_access = True
            else:
                admin_user_obj.is_vendor_access = False

            if driver == '':
                admin_user_obj.is_executive_access = True
            else:
                admin_user_obj.is_executive_access = False

            if option == '':
                admin_user_obj.is_option_access = True
            else:
                admin_user_obj.is_option_access = False

            if order == '':
                admin_user_obj.is_order_access = True
            else:
                admin_user_obj.is_order_access = False
            if admin_access == '':
                admin_user_obj.is_admin_user_access = True
            else:
                admin_user_obj.is_admin_user_access = False
            admin_user_obj.save()
        else:
            user_id = request.POST.get('edit_id')
            first_name = request.POST.get('first_name')
            user_status = request.POST.get('user_status')
            category = request.POST.get('category')
            sub_category = request.POST.get('sub_category')
            vendor = request.POST.get('vendor')
            driver = request.POST.get('driver')
            option = request.POST.get('option')
            order = request.POST.get('order')
            admin_access = request.POST.get('admin_access')
            print(category)
            print(sub_category)

            admin_user_obj = AdminUserData.objects.get(id=int(user_id))
            admin_user_obj.user.first_name = first_name
            admin_user_obj.user.isMobileVerified = user_status
            admin_user_obj.user.save()
            if category == '':
                admin_user_obj.is_category_access = True
            else:
                admin_user_obj.is_category_access = False

            if sub_category == '':
                admin_user_obj.is_sub_category_access = True
            else:
                admin_user_obj.is_sub_category_access = False

            if vendor == '':
                admin_user_obj.is_vendor_access = True
            else:
                admin_user_obj.is_vendor_access = False

            if driver == '':
                admin_user_obj.is_executive_access = True
            else:
                admin_user_obj.is_executive_access = False

            if option == '':
                admin_user_obj.is_option_access = True
            else:
                admin_user_obj.is_option_access = False

            if order == '':
                admin_user_obj.is_order_access = True
            else:
                admin_user_obj.is_order_access = False

            if admin_access == '':
                admin_user_obj.is_admin_user_access = True
            else:
                admin_user_obj.is_admin_user_access = False
            admin_user_obj.save()
        return redirect('/user/admin_user_list/')
    user_list = AdminUserData.objects.all().order_by('-created')
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'admin_user/list.html', {'user_list': user_list, 'tab': int(tab)})


@csrf_exempt
def edit_admin_user(request):
    user_id = request.GET.get('user_id')
    user_obj = AdminUserData.objects.get(id=int(user_id))
    data = {}
    data['id'] = user_obj.id
    data['first_name'] = user_obj.user.first_name
    data['is_category_access'] = user_obj.is_category_access
    data['is_sub_category_access'] = user_obj.is_sub_category_access
    data['is_vendor_access'] = user_obj.is_vendor_access
    data['is_executive_access'] = user_obj.is_executive_access
    data['is_option_access'] = user_obj.is_option_access
    data['is_order_access'] = user_obj.is_order_access
    data['is_admin_user_access'] = user_obj.is_admin_user_access
    data['is_user_access'] = user_obj.is_user_access
    data['is_user_status'] = user_obj.user.isMobileVerified
    return JsonResponse({'success': True, 'res_json': data})


def user_exists(request):
    keyword = request.GET.get('keyword')
    # user_json = {}
    is_exists = User.objects.filter(username__iexact=keyword).exists()
    return JsonResponse({'success': True,
                         'is_exists': is_exists,
                         # 'user_json': user_json
                         })


#
@login_required
def admin_change_password(request):
    user_obj = User.objects.get(id=int(request.user.id))
    errors = ''
    if request.method == 'POST':
        old_password = request.POST.get('old_password')
        password = request.POST.get('new_password')
        confirm_password = request.POST.get('confirm_password')
        logined_user = request.user
        correct_old_password = logined_user.check_password(old_password)

        if not correct_old_password:
            errors = "Incorrect old Password! Please try again"
        elif password != confirm_password:
            errors = "Password and Confirm Password should be same"
        else:
            logined_user.set_password(password)
            logined_user.save()

            return render(request, 'message.html',
                          {'message': 'Your Password has been changed successfully', 'flag': True})

    base_image_url = request.scheme + '://' + request.get_host()
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'change_password.html', {'user_obj': user_obj,
                                                    'current_user': request.user,
                                                    'base_image_url': base_image_url,
                                                    'errors': errors,
                                                    'tab': int(tab)
                                                    })


@csrf_exempt
def change_category_status(request):
    if request.method == 'POST':
        status = request.POST.get('status')
        change_id = request.POST.get('id')
        obj = CategoryData.objects.get(id=int(change_id))
        if status == 'true':
            obj.is_active = True
        else:
            obj.is_active = False
        obj.save()
    return JsonResponse({'success': True})


@csrf_exempt
def change_sub_category_status(request):
    if request.method == 'POST':
        status = request.POST.get('status')
        change_id = request.POST.get('id')
        obj = SubCategoryData.objects.get(id=int(change_id))
        if status == 'true':
            obj.is_active = True
        else:
            obj.is_active = False
        obj.save()
    return JsonResponse({'success': True})


@csrf_exempt
def change_user_status(request):
    if request.method == 'POST':
        status = request.POST.get('status')
        change_id = request.POST.get('id')
        obj = User.objects.get(id=int(change_id))
        if status == 'true':
            obj.is_active = True
        else:
            obj.is_active = False
        obj.save()
    return JsonResponse({'success': True})


def static_vendor_rating(vendor_id):
    vote = 0
    total_rating = 0
    order_queryset = OrderData.objects.filter(vendor__id=int(vendor_id), status=6)
    for obj in order_queryset:
        if obj.vendor_rating is None or '':
            obj.vendor_rating = 0
        else:
            vote += 1
            total_rating += round(float(obj.vendor_rating), 2)
    if vote == 0:
        rating = 5
    else:
        rating = round(total_rating / vote, 1)
    return rating


def static_executive_rating(executive_id):
    vote = 0
    total_rating = 0
    order_queryset = OrderData.objects.filter(delivery_boy__id=int(executive_id), status=6)
    for obj in order_queryset:
        if obj.executive_rating is None or '':
            obj.executive_rating = 0
        else:
            vote += 1
            total_rating += round(float(obj.executive_rating), 2)
    if vote == 0:
        rating = 5
    else:
        rating = round(total_rating / vote, 1)
    return rating


@login_required
def vendor_report(request, vendor_id):
    vendor_orders = OrderData.objects.filter(vendor__id=int(vendor_id), status=6)
    serializer = OrderListSerializer(vendor_orders, many=True)
    vendor_obj = VendorData.objects.get(id=int(vendor_id))
    return render(request, 'report/vendor_report.html', {
        "order_list": serializer.data,
        'vendor_id': int(vendor_id),
        "email": vendor_obj.email
    })


@login_required
def executive_report(request, executive_id):
    executive_orders = OrderData.objects.filter(delivery_boy__id=int(executive_id), status=6)
    serializer = OrderListSerializer(executive_orders, many=True)
    executive_obj = ExecutiveData.objects.get(id=int(executive_id))
    return render(request, 'report/executive_report.html', {
        "order_list": serializer.data,
        'executive_id': int(executive_id),
        "email": executive_obj.email
    })


def vendor_report_download(request, vendor_id):
    vendor_obj = VendorData.objects.get(id=int(vendor_id))
    main_list = []
    response = HttpResponse(content_type='text/csv')

    response['Content-Disposition'] = 'attachment; filename="{0}"'.format(
        str(vendor_obj.shop_name) + '_report.csv')
    header_list = ['Order ID', 'Date', 'Partner', 'Shop Name', 'Sub Total', 'Partner Amount', 'Meat4all Percentage',
                   'Meat4all Amount']
    main_list.append(header_list)
    vendor_orders = OrderData.objects.filter(vendor__id=int(vendor_id), status=6)
    for obj in vendor_orders:
        temp_list = [
            # counter,
            obj.custom_id,
            str(obj.created.date()),
            str(obj.vendor.user.first_name) + ' ' + str(obj.vendor.user.last_name),
            obj.vendor.shop_name,
            '£ ' + str(obj.total),
            '£ ' + str(obj.vendor_amount),
            str(obj.vendor_amount_per) + '%',
            '£ ' + str(obj.meat4all_amount),
        ]
        main_list.append(temp_list)
    writer = csv.writer(response)
    for index, data in enumerate(main_list):
        writer.writerow(data)

    return response


def executive_report_download(request, executive_id):
    executive_obj = VendorData.objects.get(id=int(executive_id))
    main_list = []
    response = HttpResponse(content_type='text/csv')

    response['Content-Disposition'] = 'attachment; filename="{0}"'.format(
        str(executive_obj.user.first_name) + '_report.csv')
    header_list = ['Order ID', 'Date', 'Driver', 'Delivery Charge', 'Driver Amount', 'Meat4all Percentage',
                   'Meat4all Amount']
    main_list.append(header_list)
    executive_orders = OrderData.objects.filter(delivery_boy__id=int(executive_id), status=6)
    for obj in executive_orders:
        temp_list = [
            # counter,
            obj.custom_id,
            str(obj.created.date()),
            str(obj.delivery_boy.user.first_name) + ' ' + str(obj.delivery_boy.user.last_name),
            '£ ' + str(obj.mile_charge),
            '£ ' + str(obj.driver_amount),
            str(obj.driver_amount_per) + '%',
            '£ ' + str(obj.meat4all_driver_amount),

        ]
        main_list.append(temp_list)
    writer = csv.writer(response)
    for index, data in enumerate(main_list):
        writer.writerow(data)

    return response


def send_report(request):
    email = request.GET.get('email')
    sender_type = request.GET.get('sender_type')
    link = request.GET.get('url')
    if sender_type == 'vendor':
        # obj = VendorData.objects.get(id=int(sender_id))
        # email = obj.user.email
        message_for_mail = 'You Report are given below:-'

    else:
        # obj = ExecutiveData.objects.get(id=int(sender_id))
        # email = obj.user.email
        message_for_mail = 'You Report are given below:-'
    link = 'https://admin.meats4all.co.uk' + str(link)
    bottom_text = '\nThank you for your continued support and business.'
    signature = '\nTeam Meats4all'

    html_content = render_to_string('email_template.html',
                                    {'text': message_for_mail,
                                     'link': link,
                                     # 'order_obj': order_obj,
                                     'bottom_text': bottom_text,
                                     'signature': signature
                                     })

    try:

        send_email(to=email, subject="Report", body=html_content)
    except Exception as e:
        print(str(e))

    return JsonResponse({'success': True})
