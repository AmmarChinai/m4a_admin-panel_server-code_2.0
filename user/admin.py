from django.contrib import admin

# Register your models here.
from user.models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'gcm_token', 'created', 'modified']


admin.site.register(User, UserAdmin)


class OtpDataAdmin(admin.ModelAdmin):
    list_display = ['user', 'code']


admin.site.register(OtpData, OtpDataAdmin)


class VendorDataAdmin(admin.ModelAdmin):
    list_display = ['user', 'shop_name', 'latitude', 'longitude']


admin.site.register(VendorData, VendorDataAdmin)


class ExecutiveDataAdmin(admin.ModelAdmin):
    list_display = ['user', 'latitude', 'longitude']


admin.site.register(ExecutiveData, ExecutiveDataAdmin)


class VendorImageDataAdmin(admin.ModelAdmin):
    list_display = ['vendor', 'image']


admin.site.register(VendorImageData, VendorImageDataAdmin)


class VendorCertificateDataAdmin(admin.ModelAdmin):
    list_display = ['vendor', 'file', 'start', 'end']


admin.site.register(VendorCertificateData, VendorCertificateDataAdmin)


class OptionDataAdmin(admin.ModelAdmin):
    list_display = ['name', ]


admin.site.register(OptionData, OptionDataAdmin)


class AddressDataAdmin(admin.ModelAdmin):
    list_display = ['title', 'user']


admin.site.register(AddressData, AddressDataAdmin)


class AdminUserDataAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_category_access', 'is_sub_category_access']


admin.site.register(AdminUserData, AdminUserDataAdmin)


class ChargeSettingDataAdmin(admin.ModelAdmin):
    list_display = ['packing_charge_per', 'cancel_charge', 'distance',
                    'tip_charge']


admin.site.register(ChargeSettingData, ChargeSettingDataAdmin)


class BannerDataAdmin(admin.ModelAdmin):
    list_display = ['title', 'image']


admin.site.register(BannerData, BannerDataAdmin)
