from django.db import models

# Create your models here.
# from payment.models import Invoice
from delivey_app.managres import LocationManager
from payment.models import Invoice
from user.models import VendorData, User, ExecutiveData

STATUS = ((1, 'Created'),
          (2, 'Accepted'),
          (3, 'Rejected'),
          (4, 'In-Progress'),
          (5, 'Picked Up'),
          (6, 'Delivered'),
          (7, 'Cancelled'),
          (8, 'Ready To Dispatch'),
          )

PAYMENT_STATUS = ((1, 'Pending'),
                  (2, 'Success'),
                  (3, 'Failed'))

PAYOUT_STATUS = ((1, 'Pending'),
                  (2, 'Cancelled'),
                  (3, 'Complete'),
                  (4, 'Under Review'))


class SettingData(models.Model):
    key = models.CharField(max_length=250)
    value = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.key


class CategoryData(models.Model):
    name = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.name


class SubCategoryData(models.Model):
    category = models.ForeignKey(CategoryData, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=250)
    is_active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.name


class VendorSubCategoryData(models.Model):
    sub_category = models.ForeignKey(SubCategoryData, on_delete=models.CASCADE, null=True, blank=True)
    vendor = models.ForeignKey(VendorData, on_delete=models.CASCADE, null=True, blank=True)
    price = models.CharField(max_length=255, default=0, null=True, blank=True)
    offer_price = models.CharField(max_length=255, default=0, null=True, blank=True)
    quantity = models.CharField(max_length=255, default=0, null=True, blank=True)
    unit = models.CharField(max_length=50, default='kg')
    is_available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.sub_category.name


class VendorOfferData(models.Model):
    vendor = models.ForeignKey(VendorData, on_delete=models.CASCADE, null=True, blank=True)
    code = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    percentage = models.CharField(max_length=255, default=0, null=True, blank=True)
    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)
    is_available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.vendor.shop_name


class OrderData(models.Model):
    invoice = models.ForeignKey(
        Invoice,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        related_name='user')

    objects = LocationManager()
    status = models.PositiveIntegerField(choices=STATUS, default=1)
    vendor = models.ForeignKey(VendorData, null=True, on_delete=models.CASCADE, blank=True)
    delivery_boy = models.ForeignKey(ExecutiveData, on_delete=models.SET_NULL, null=True, related_name='delivery_boy')
    is_home_delivery = models.BooleanField(default=True)
    delivery_image = models.ImageField(upload_to='', null=True, blank=True)
    booking_slot = models.TextField(default='', blank=True)

    title = models.TextField(max_length=500, null=True)
    drop_address = models.TextField(max_length=500, null=True)
    drop_latitude = models.DecimalField(max_digits=9, decimal_places=7, blank=True, null=True)
    drop_longitude = models.DecimalField(max_digits=9, decimal_places=7, blank=True, null=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=7, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=7, blank=True, null=True)
    notes = models.TextField(max_length=1000, null=True, blank=True)
    custom_id = models.TextField(null=True, blank=True)
    offer_id = models.CharField(max_length=100, default=0,null=True, blank=True)

    tip = models.CharField(max_length=100, default=0, null=True, blank=True)

    discount = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    supplier_fee = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    packing_charge = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    mile_charge = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    driver_charge = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    tax = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    total = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    total_bill = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    skype_amount = models.CharField(max_length=100, default=0, null=True, blank=True)
    sub_total_1 = models.CharField(max_length=100, default=0, null=True, blank=True)
    service_fee_deducted = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    delivery_charge_deducted = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    customer_transaction_value = models.CharField(max_length=100, default=0, null=True, blank=True)
    meat4all_platform_fee_20_percent = models.CharField(max_length=100, default=0, null=True, blank=True)
    sub_total_2 = models.CharField(max_length=100, default=0, null=True, blank=True)
    stripe_payout_charge_vendor = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    vendor_payout = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    stripe_payout_charge_driver = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    driver_payout = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    platform_fee_driver = models.DecimalField(max_digits=7, decimal_places=2, default=0, null=True, blank=True)
    total_m4a_amount = models.CharField(max_length=100, default=0, null=True, blank=True)
    net_value_m4a = models.CharField(max_length=100, default=0, null=True, blank=True)

    meat4all_amount = models.CharField(max_length=100, default=0, null=True, blank=True)
    # meat4all_amount_per = models.CharField(max_length=100, default=0, null=True, blank=True)
    vendor_amount = models.CharField(max_length=100, default=0, null=True, blank=True)
    vendor_amount_per = models.CharField(max_length=100, default=0, null=True, blank=True)
    driver_amount_per = models.CharField(max_length=100, default=0, null=True, blank=True)
    driver_amount = models.CharField(max_length=100, default=0, null=True, blank=True)

    meat4all_driver_amount = models.CharField(max_length=100, default=0, null=True, blank=True)

    stripe_payment_id = models.CharField(max_length=250, null=True, blank=True)
    firebase_key = models.CharField(max_length=100, null=True, blank=True)
    cancel_reason = models.TextField(max_length=1000, null=True, blank=True)
    executive_rating = models.PositiveSmallIntegerField(null=True, blank=True)
    executive_feedback = models.TextField(max_length=1000, null=True, blank=True)
    vendor_rating = models.PositiveSmallIntegerField(null=True, blank=True)
    vendor_feedback = models.TextField(max_length=1000, null=True, blank=True)
    assigned_executive = models.BooleanField(default=False)
    payment_status = models.PositiveIntegerField(choices=PAYMENT_STATUS, default=1)
    payout_status = models.PositiveIntegerField(choices=PAYOUT_STATUS, default=1)
    driver_payout_status = models.PositiveIntegerField(choices=PAYOUT_STATUS, default=1)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    order_date = models.DateField(null=True)

    def __str__(self):
        return "%s" % self.id


class PaymentHistoryData(models.Model):

    objects = LocationManager()
    payout_reference = models.TextField(null=True, blank=True)
    contact_id = models.TextField(null=True, blank=True)
    total_amount = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    contact_email = models.TextField(null=True, blank=True)
    contact_name = models.TextField(null=True, blank=True)
    contact_phone_number = models.TextField(null=True, blank=True)
    payout_date = models.TextField(null=True, blank=True)
    payout_type = models.TextField(null=True, blank=True)
    
    def __str__(self):
        return "%s" % self.id



class OrderServices(models.Model):
    service = models.ForeignKey(
        VendorSubCategoryData,
        null=True,
        on_delete=models.DO_NOTHING,
    )

    order = models.ForeignKey(
        OrderData,
        null=True,
        on_delete=models.DO_NOTHING)

    quantity = models.CharField(max_length=30, null=True)
    note = models.TextField(default='', blank=True)
    price = models.CharField(max_length=30, null=True)
    unit = models.CharField(max_length=30, null=True)
    is_removed = models.BooleanField(default=False)

    def __str__(self):
        return '%s %s' % (self.id, self.service.sub_category.name)


