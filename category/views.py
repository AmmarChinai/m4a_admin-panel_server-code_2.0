import json

import stripe
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from flask import jsonify

from category.forms import SubCategoryForm, UpdateSubCategoryForm
from category.models import CategoryData, SubCategoryData, VendorSubCategoryData, VendorOfferData, OrderServices, \
    OrderData, SettingData, PaymentHistoryData
from category.serialize import OrderListSerializer, OrderDetailsSerializer, VendorOfferSerializer, \
    ExecutiveListSerializer
from delivey_app import settings
from user.models import User, VendorData, ExecutiveData, ChargeSettingData
from user.views import jwt_decode, notify_user, send_sms, send_email


def category_data_dashboard(request):
    if request.method == 'POST':
        if 'add' in request.POST:
            name = request.POST.get('name')
            image = request.FILES.get('image')
            CategoryData.objects.create(name=name, image=image)
        elif 'edit' in request.POST:
            name = request.POST.get('name')
            image = request.FILES.get('image')
            category_id = request.POST.get('category_id')
            obj = CategoryData.objects.get(id=int(category_id))
            if image is not None:
                obj.image = image
            obj.name = name
            obj.save()
        else:
            category_id = request.POST.get('category_id')
            obj = CategoryData.objects.get(id=int(category_id))
            obj.subcategorydata_set.all().delete()
            obj.delete()
        return redirect('/category/category_list/')
    base_image_url = request.scheme + '://' + request.get_host()
    category_list = []
    category_queryset = CategoryData.objects.all().order_by('-created')
    for obj in category_queryset:
        if obj.image != '':
            image = base_image_url + '/media/' + str(obj.image)
        else:
            image = ''
        data = {
            'id': obj.id,
            'name': obj.name,
            'is_active': obj.is_active,
            'image': image
        }
        category_list.append(data)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'category.html', {'category_list': category_list, 'tab': int(tab)})


def category_data(request):
    base_image_url = request.scheme + '://' + request.get_host()
    category_list = static_category_list(base_image_url)
    return JsonResponse({'success': True, 'message': 'Category List', 'category_list': category_list})


def static_category_list(base_image_url):
    category_list = []
    category_queryset = CategoryData.objects.filter(is_active=True)
    for obj in category_queryset:
        if obj.image != '':
            image = base_image_url + '/media/' + str(obj.image)
        else:
            image = ''
        data = {
            'id': obj.id,
            'name': obj.name,
            'is_active': obj.is_active,
            'image': image
        }
        category_list.append(data)
    return category_list


def sub_category_data_dashboard(request):
    if request.method == 'POST':
        if 'add' in request.POST:
            image = request.FILES.get('image')
            cat_form = SubCategoryForm(request.POST)
            sub_category = cat_form.save()
            sub_category.image = image
            sub_category.save()
        elif 'edit' in request.POST:
            image = request.FILES.get('image')
            category_id = request.POST.get('category_id')
            obj = SubCategoryData.objects.get(id=int(category_id))
            cat_form = UpdateSubCategoryForm(request.POST, instance=obj)
            sub_category = cat_form.save()
            sub_category.save()
            if image is not None:
                sub_category.image = image
            obj.save()
        else:
            category_id = request.POST.get('category_id')
            obj = SubCategoryData.objects.get(id=int(category_id))
            obj.delete()
        return redirect('/category/sub_category_list/')
    base_image_url = request.scheme + '://' + request.get_host()
    sub_category_list = []
    sub_category_queryset = SubCategoryData.objects.all().order_by('-created')
    for obj in sub_category_queryset:
        if obj.image != '':
            image = base_image_url + '/media/' + str(obj.image)
        else:
            image = ''
        data = {
            'id': obj.id,
            'category': obj.category.name,
            'category_id': obj.category.id,
            'name': obj.name,
            'is_active': obj.is_active,
            'image': image

        }
        sub_category_list.append(data)

    paginator = Paginator(sub_category_list, 20)
    page = request.GET.get('page')

    try:
        sub_category_list = paginator.page(page)
    except PageNotAnInteger:
        sub_category_list = paginator.page(1)
    except EmptyPage:
        sub_category_list = paginator.page(paginator.num_pages)
    category_list = CategoryData.objects.filter(is_active=True)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'sub_category_list.html', {'sub_category_list': sub_category_list,
                                                      'tab': int(tab),
                                                      'category_list': category_list})


def sub_category_data(request):
    base_image_url = request.scheme + '://' + request.get_host()
    sub_category_list = static_sub_category_list(base_image_url)
    return JsonResponse({'success': True, 'message': 'Sub Category List', 'sub_category_list': sub_category_list})


def category_sub_category_data(request, category_id, vendor_id):
    sub_category_list = []
    sub_category_vendor_list = []
    base_image_url = request.scheme + '://' + request.get_host()
    vendor_obj = VendorData.objects.get(id=int(vendor_id))

    sub_category_vendor_queryset = VendorSubCategoryData.objects.filter(vendor=vendor_obj)
    try:
        for i in sub_category_vendor_queryset:
            sub_category_vendor_list.append(i.sub_category.id)
        # print(sub_category_vendor_list)
    except Exception as e:
        print(str(e))

    sub_category_queryset = SubCategoryData.objects.filter(category__id=int(category_id))
    for obj in sub_category_queryset:
        data = {}
        if obj.id in sub_category_vendor_list:
            vendor_sub_obj = VendorSubCategoryData.objects.get(sub_category_id=obj.id, vendor=vendor_obj)
            data['is_available'] = vendor_sub_obj.is_available
            data['price'] = vendor_sub_obj.price
            data['unit'] = vendor_sub_obj.unit
            data['quantity'] = vendor_sub_obj.quantity
            data['offer_price'] = vendor_sub_obj.offer_price
        else:
            data['is_available'] = False
            data['price'] = ''
            data['unit'] = ''
            data['quantity'] = ''
            data['offer_price'] = ''
        if obj.image != '':
            image = base_image_url + '/media/' + str(obj.image)
        else:
            image = ''
        data['sub_category_id'] = obj.id
        data['category'] = obj.category.name
        data['name'] = obj.name
        data['image'] = image
        sub_category_list.append(data)
    return JsonResponse({'success': True, 'message': 'Sub Category List', 'sub_category_list': sub_category_list})


def static_sub_category_list(base_image_url):
    sub_category_list = []
    sub_category_queryset = SubCategoryData.objects.filter(is_active=True).order_by('-created')
    for obj in sub_category_queryset:
        if obj.image != '':
            image = base_image_url + '/media/' + str(obj.image)
        else:
            image = ''
        data = {
            'id': obj.id,
            'category': obj.category.name,
            'category_id': obj.category.id,
            'name': obj.name,
            'is_active': obj.is_active,
            'image': image

        }
        sub_category_list.append(data)
    return sub_category_list


# def static_bill_function(miles):
#     settings_obj = ChargeSettingData.objects.first()
#     if miles <= 3:
#         miles_charge = settings_obj.upto_3_mile
#     elif miles > 3 and miles <= 5:
#         miles_charge = settings_obj.from_3to5_mile
#     elif miles > 5 and miles <= 10:
#         miles_charge = settings_obj.from_3to5_mile
#     else:
#         miles_charge = settings_obj.from_3to5_mile
#     print("mile_charge ====> ", miles_charge)
#     print("miles charge with round ==>>", float(miles_charge))
#     # return round(float(miles_charge))
#     return float(miles_charge)


def static_bill_function(miles):
    settings_obj = ChargeSettingData.objects.first()
    if miles <= 3:
        miles_charge = settings_obj.upto_3_mile
        driver_charge = settings_obj.driver_charge_upto_3_mile
    elif miles > 3 and miles <= 5:
        miles_charge = settings_obj.from_3to5_mile
        driver_charge = settings_obj.driver_charge_from_3to5_mile
    elif miles > 5 and miles <= 10:
        miles_charge = settings_obj.from_5to10_mile
        driver_charge = settings_obj.driver_charge_from_5to10_mile
    else:
        miles_charge = settings_obj.from_10to30_mile
        driver_charge = settings_obj.driver_charge_from_10to30_mile
    return (float(miles_charge)), (float(driver_charge))


@csrf_exempt
def add_cart(request):
    cart_list = []
    if request.method == 'POST':
        data = json.loads(request.body)
        # token = data['token']
        miles = data['miles']
        offer_id = data['offer_id']
        vendor_id = data['vendor_id']
        # decoded_token = jwt_decode(token)
        # mobile = decoded_token['mobile']
        # user = User.objects.filter(username=mobile).first()
        # if not user:
        #     return JsonResponse({'success': False, 'message': 'Invalid User'})
        sub_category_list = data['sub_category_list']
        total = 0
        total_quatity = 0
        sub_total = 0
        offer_less = 0
        supplier_fee = 0
        vendor_obj = VendorData.objects.get(id=int(vendor_id))

        for obj in sub_category_list:
            data_json = {}
            item_price = 0
            total_quatity += float(obj['quantity'])

            # data['quantity'] = data['quantity']
            sub_cat_obj = VendorSubCategoryData.objects.get(id=int(obj['sub_category_id']))
            print(sub_cat_obj.price, 'sub_cat_obj.offer_priceprice')
            print(type(sub_cat_obj.offer_price), 'ssssssssssssaaaaaaaaaaaaaaaaaaaaaaaaaaasssss')

            if float(sub_cat_obj.offer_price) != 0.0:
                item_price = float(obj['quantity']) * float(sub_cat_obj.offer_price)
            else:
                item_price = float(obj['quantity']) * float(sub_cat_obj.price)
            print(item_price, 'lllllllllllllllllllllllllll')
            data_json['sub_cat_id'] = sub_cat_obj.id
            data_json['price'] = sub_cat_obj.price
            data_json['quantity'] = obj['quantity']
            data_json['note'] = obj['note']
            data_json['offer_price'] = sub_cat_obj.offer_price
            data_json['unit'] = sub_cat_obj.unit
            data_json['sub_category_name'] = sub_cat_obj.sub_category.name
            data_json['image'] = str(sub_cat_obj.sub_category.image)
            data_json['category_name'] = sub_cat_obj.sub_category.category.name
            data_json['category_id'] = sub_cat_obj.sub_category.category.id
            data_json['vendor_id'] = sub_cat_obj.vendor.id
            data_json['vendor_name'] = sub_cat_obj.vendor.shop_name
            data_json['total_item_price'] = item_price

            sub_total += float(item_price)

            cart_list.append(data_json)
        if int(offer_id) != 0:
            offer_obj = VendorOfferData.objects.get(id=int(offer_id))
            offer_less = round((sub_total * float(offer_obj.percentage)) / 100, 2)
        vendor_offer = VendorOfferData.objects.filter(vendor=vendor_obj)
        serializer = VendorOfferSerializer(vendor_offer, many=True)
        settings_obj = ChargeSettingData.objects.first()
        delivery_charge = static_bill_function(miles=float(miles))
        del_charge = delivery_charge[0]
        print("Delivery Charge first func==>", delivery_charge[0])

        packing_charge_per = settings_obj.packing_charge_per
        packing_charge = packing_charge_per

        # if float(total_quatity) > 4:
        #     if float(total_quatity) % 4 == 0:
        #         sub_quatity_diffrence = float(total_quatity) // 4
        #     else:
        #         sub_quatity_diffrence = 1 + float(total_quatity) // 4
        # else:
        #     sub_quatity_diffrence = 1
        #
        # packing_charge = sub_quatity_diffrence * float(packing_charge_per)

        total_bill = (float(sub_total) + float(packing_charge) + float(del_charge) - float(offer_less))

        if total_bill >= 40:
            if miles <= 3:
                del_charge = 0
            else:
                del_charge = delivery_charge[0]
        else:
            del_charge = delivery_charge[0]

        total_bill = (float(sub_total) + float(packing_charge) + float(del_charge) - float(offer_less))

        return JsonResponse({'success': True, 'cart_list': cart_list, 'total_bill': float(total_bill),
                             'total': total,
                             'sub_total': sub_total,
                             'vendor_id': vendor_obj.id,
                             'offers': serializer.data,
                             'vendor_shop_name': vendor_obj.shop_name,
                             'vendor_address': vendor_obj.address,
                             'supplier_fee': supplier_fee,
                             'offer_discount': offer_less,
                             'offer_id': offer_id,
                             'delivery_charge': del_charge,
                             'packing_charge': packing_charge,
                             })


@csrf_exempt
def place_order(request):
    cart_list = []
    if request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        address = data['address']
        title = data['title']
        lat = data['latitude']
        long = data['longitude']
        is_home_delivery = data['is_home_delivery']
        time_slot = data['time_slot']
        order_date = data['order_date']
        miles = data['miles']
        # driver_charge = data['driver_charge']
        notes = data['notes']
        offer_id = data['offer_id']
        vendor_id = data['vendor_id']
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        sub_category_list = data['sub_category_list']
        sub_total = 0
        total_quatity = 0
        offer_less = 0
        supplier_fee = 0
        vendor_obj = VendorData.objects.get(id=int(vendor_id))
        order_obj = OrderData.objects.create(user=user, vendor=vendor_obj, drop_address=address, title=title,
                                             drop_latitude=lat,
                                             drop_longitude=long, status=1, is_home_delivery=is_home_delivery,
                                             notes=notes, booking_slot=time_slot, order_date=order_date)

        for obj in sub_category_list:
            data_json = {}
            item_price = 0
            total_quatity += float(obj['quantity'])
            sub_cat_obj = VendorSubCategoryData.objects.get(id=int(obj['sub_category_id']))
            print(sub_cat_obj.offer_price, 'aaaaaaaaaaaaaaaaaaaaaaaaaa')

            if float(sub_cat_obj.offer_price) != 0.0:
                item_price = float(obj['quantity']) * float(sub_cat_obj.offer_price)
            else:
                item_price = float(obj['quantity']) * float(sub_cat_obj.price)
            data_json['price'] = sub_cat_obj.price
            data_json['quantity'] = obj['quantity']
            data_json['note'] = obj['note']
            data_json['offer_price'] = sub_cat_obj.offer_price
            data_json['unit'] = sub_cat_obj.unit
            data_json['sub_category_name'] = sub_cat_obj.sub_category.name
            data_json['image'] = str(sub_cat_obj.sub_category.image)
            data_json['category_name'] = sub_cat_obj.sub_category.category.name
            data_json['category_id'] = sub_cat_obj.sub_category.category.id
            # data_json['vendor_id'] = sub_cat_obj.vendor.id
            # data_json['vendor_name'] = sub_cat_obj.vendor.shop_name
            data_json['total_item_price'] = item_price

            sub_total += float(item_price)
            cart_list.append(data_json)

            OrderServices.objects.create(order=order_obj, service=sub_cat_obj, quantity=obj['quantity'],
                                         unit=sub_cat_obj.unit, price=item_price, note=obj['note'])

        if int(offer_id) != 0:
            offer_obj = VendorOfferData.objects.get(id=int(offer_id))
            offer_less = round((sub_total * float(offer_obj.percentage)) / 100, 2)
        custom_id = 'M4A00' + str(order_obj.id)
        settings_obj = ChargeSettingData.objects.first()
        delivery_charge = static_bill_function(miles=float(miles))

        # settings_obj = ChargeSettingData.objects.first()
        #
        # delivery_charge = order_obj.mile_charge
        del_charge_1 = delivery_charge[0]
        print("delivery====>>>", delivery_charge[0])
        print("delivery====>>>", delivery_charge[1])

        if del_charge_1 <= 3.00:
            driver_charge = settings_obj.driver_charge_upto_3_mile
        elif 3 < del_charge_1 <= 5.00:
            driver_charge = settings_obj.driver_charge_from_3to5_mile
        elif 5 < del_charge_1 <= 10.00:
            driver_charge = settings_obj.driver_charge_from_5to10_mile
        else:
            driver_charge = settings_obj.driver_charge_from_10to30_mile

        print("Driver charge ==>>", driver_charge)

        packing_charge_per = settings_obj.packing_charge_per
        packing_charge = packing_charge_per

        # if float(total_quatity) > 4:
        #     if float(total_quatity) % 4 == 0:
        #         sub_quatity_diffrence = float(total_quatity) // 4
        #     else:
        #         sub_quatity_diffrence = 1 + float(total_quatity) // 4
        # else:
        #     sub_quatity_diffrence = 1
        # print(sub_quatity_diffrence, 'sub_quatity_diffrence')
        # print(packing_charge_per, 'packing_charge_per')
        # packing_charge = sub_quatity_diffrence * float(packing_charge_per)

        # if 3 < delivery_charge <= 5:
        #     delivery_charge = 0

        # if not is_home_delivery:
        #     delivery_charge = 0

        print("calculation started here====>")
        total_bill = (float(sub_total) + float(packing_charge) + float(del_charge_1) - float(offer_less))

        # if total_bill >= 40:
        #     if miles <= 3:
        #         delivery_charge = 0

        if not is_home_delivery:
            del_charge_1 = 0
        else:
            if total_bill >= 40:
                if miles <= 3:
                    del_charge_1 = 0
                    print("order is under 3 miles==>>", del_charge_1)
                else:
                    order_obj.mile_charge = del_charge_1
            else:
                order_obj.mile_charge = del_charge_1

        total_bill = (float(sub_total) + float(packing_charge) + float(del_charge_1) - float(offer_less))

        print("miles set==>>", miles)
        print(total_bill, "total bill order ===>>")
        skype_amount = ((float(total_bill) * 1.40 / 100) + 0.20)
        print(skype_amount, " stripe amount is ==>> ")

        # sub_total_1 = (total_amount)-(stripe_amount)
        sub_total_1 = (float(total_bill) - float(skype_amount))
        print(sub_total_1, "sub total 1 is ===>> ")
        print(packing_charge, "Packing charge:===")
        print(del_charge_1, "Delivery charge:===")

        # M4A platform Service Fee £1.99 (Deducted from Sub total 1)
        # [packing charge and service fee are same]
        service_fee_deducted = (float(sub_total_1) - float(packing_charge))
        print("service_fee_deducted ==>>", service_fee_deducted)

        # Delivery charge Deducted [from service_fee_deducted]
        if is_home_delivery:
            delivery_charge_deducted = (float(service_fee_deducted) - float(del_charge_1))
            print("Delivery charge Deducted ==>>", delivery_charge_deducted)
            order_obj.delivery_charge_deducted = round(delivery_charge_deducted, 2)
        else:
            del_charge_1 = 0
            delivery_charge_deducted = (float(service_fee_deducted) - float(del_charge_1))
            print("Delivery charge Deducted ==>>", delivery_charge_deducted)
            order_obj.delivery_charge_deducted = round(delivery_charge_deducted, 2)

        # customer_transaction_value  = (sub_total_1) - (service_fee[1.99]) - (delivery_charge[2.99])
        customer_transaction_value = (float(sub_total_1) - float(packing_charge) - float(del_charge_1))
        print(customer_transaction_value, "customer_transaction_value")

        # meat4all_platform_fee_20_percent
        meat4all_per = vendor_obj.meat4all_per
        meat4all_platform_fee_20_percent = (float(customer_transaction_value) * float(meat4all_per) / 100)
        print(meat4all_per, "M4A % is ===> ")
        print(meat4all_platform_fee_20_percent, "M4A platform charge ===> ")

        # Sub_total_2 = (80% of customer_transaction_value) + (delivery_charge)
        sub_total_2 = (float(customer_transaction_value) - float(meat4all_platform_fee_20_percent))
        print(sub_total_2, "sub_total_2 is ===>>")

        # Stripe payout charge for vendors [0.25 + 0.1]
        stripe_payout_charge_vendor = ((float(sub_total_2) * 0.25) / 100) + 0.1
        print("stripe_payout_charge ==>>", stripe_payout_charge_vendor)

        # Vendor payout [ (sub total 2) - (stripe_payout_charge) ]
        vendor_payout = (float(sub_total_2) - float(stripe_payout_charge_vendor))
        print("vendor_payout ==>>", vendor_payout)

        # Stripe payout charge for Driver [0.25 + 0.1]
        if is_home_delivery:
            stripe_payout_charge_driver = ((float(driver_charge) * 0.25) / 100) + 0.1
            print("stripe_payout_charge_driver==>>", stripe_payout_charge_driver)
            order_obj.stripe_payout_charge_driver = round(stripe_payout_charge_driver, 2)

            # Drive payout [ (driver charge) - (stripe_payout_charge_driver)]
            driver_payout = (float(driver_charge) - float(stripe_payout_charge_driver))
            print("driver_payout==>>", driver_payout)
            order_obj.driver_payout = round(driver_payout, 2)
        else:
            driver_charge = 0

        # M4A Platform fee Driver
        if del_charge_1 != 0:
            platform_fee_driver = (float(del_charge_1) - float(driver_charge))
            print("platform_fee_driver==>>", platform_fee_driver)
            order_obj.platform_fee_driver = round(platform_fee_driver, 2)

        # (total_m4a_amount) This is Total meats4all amount
        total_m4a_amount = (float(packing_charge) + float(meat4all_platform_fee_20_percent)
                            + float(del_charge_1))
        print(total_m4a_amount, "This is Total meats4all amount ===> ")

        # Net value m4a
        net_value_m4a = float(total_m4a_amount) - float(driver_charge)
        print(driver_charge, "driver_charge as per miles ==>> ")
        print(net_value_m4a, "net_value_m4a ====>>")

        # Driver payout is here

        order_obj.skype_amount = round(skype_amount, 2)
        order_obj.total_bill = round(total_bill, 2)
        order_obj.supplier_fee = supplier_fee
        order_obj.discount = offer_less
        order_obj.total = round(sub_total, 2)
        order_obj.mile_charge = del_charge_1
        order_obj.driver_charge = driver_charge
        order_obj.packing_charge = packing_charge
        order_obj.custom_id = custom_id
        order_obj.offer_id = offer_id
        order_obj.latitude = order_obj.vendor.latitude
        order_obj.longitude = order_obj.vendor.longitude
        order_obj.sub_total_1 = round(sub_total_1, 2)
        order_obj.service_fee_deducted = round(service_fee_deducted, 2)
        order_obj.customer_transaction_value = round(customer_transaction_value, 2)
        order_obj.meat4all_platform_fee_20_percent = round(meat4all_platform_fee_20_percent, 2)
        order_obj.sub_total_2 = round(sub_total_2, 2)
        order_obj.stripe_payout_charge_vendor = round(stripe_payout_charge_vendor, 2)
        order_obj.vendor_payout = round(vendor_payout, 2)
        order_obj.total_m4a_amount = round(total_m4a_amount, 2)
        order_obj.net_value_m4a = round(net_value_m4a, 2)
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'created'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(vendor_obj.shop_name) + ", You have a New " \
                                                                "order with order ID : " + str(
                order_obj.custom_id) + ' .'
            notify_user(
                user=vendor_obj.user,
                message=notify_text,
                title='New Order Received',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({'success': True, 'cart_list': cart_list, 'total_bill': float(total_bill),
                             'total': sub_total,
                             'offer_id': offer_id,
                             'vendor_id': vendor_obj.id,
                             'shop_name': vendor_obj.shop_name,
                             'order_id': order_obj.id,
                             'supplier_fee': supplier_fee,
                             'offer_discount': offer_less,
                             'packing_charge': packing_charge})
    # cart_list = []
    # if request.method == 'POST':
    #     data = json.loads(request.body)
    #     token = data['token']
    #     address = data['address']
    #     title = data['title']
    #     lat = data['latitude']
    #     long = data['longitude']
    #     is_home_delivery = data['is_home_delivery']
    #     time_slot = data['time_slot']
    #     order_date = data['order_date']
    #     miles = data['miles']
    #     notes = data['notes']
    #     offer_id = data['offer_id']
    #     vendor_id = data['vendor_id']
    #     decoded_token = jwt_decode(token)
    #     mobile = decoded_token['mobile']
    #     user = User.objects.filter(username=mobile).first()
    #     if not user:
    #         return JsonResponse({'success': False, 'message': 'Invalid User'})
    #     sub_category_list = data['sub_category_list']
    #     sub_total = 0
    #     total_quatity = 0
    #     offer_less = 0
    #     supplier_fee = 0
    #     vendor_obj = VendorData.objects.get(id=int(vendor_id))
    #     order_obj = OrderData.objects.create(user=user, vendor=vendor_obj, drop_address=address,
    #                                          title=title, drop_latitude=lat, drop_longitude=long,
    #                                          status=1, is_home_delivery=is_home_delivery, notes=notes,
    #                                          booking_slot=time_slot, order_date=order_date)
    #
    #     for obj in sub_category_list:
    #         data_json = {}
    #         item_price = 0
    #         total_quatity += float(obj['quantity'])
    #         sub_cat_obj = VendorSubCategoryData.objects.get(id=int(obj['sub_category_id']))
    #         print(sub_cat_obj.offer_price, 'aaaaaaaaaaaaaaaaaaaaaaaaaa')
    #
    #         if float(sub_cat_obj.offer_price) != 0.0:
    #             item_price = float(obj['quantity']) * float(sub_cat_obj.offer_price)
    #         else:
    #             item_price = float(obj['quantity']) * float(sub_cat_obj.price)
    #         data_json['price'] = sub_cat_obj.price
    #         data_json['quantity'] = obj['quantity']
    #         data_json['note'] = obj['note']
    #         data_json['offer_price'] = sub_cat_obj.offer_price
    #         data_json['unit'] = sub_cat_obj.unit
    #         data_json['sub_category_name'] = sub_cat_obj.sub_category.name
    #         data_json['image'] = str(sub_cat_obj.sub_category.image)
    #         data_json['category_name'] = sub_cat_obj.sub_category.category.name
    #         data_json['category_id'] = sub_cat_obj.sub_category.category.id
    #         # data_json['vendor_id'] = sub_cat_obj.vendor.id
    #         # data_json['vendor_name'] = sub_cat_obj.vendor.shop_name
    #         data_json['total_item_price'] = item_price
    #
    #         sub_total += float(item_price)
    #         cart_list.append(data_json)
    #
    #         OrderServices.objects.create(order=order_obj, service=sub_cat_obj, quantity=obj['quantity'],
    #                                      unit=sub_cat_obj.unit, price=item_price, note=obj['note'])
    #
    #     if int(offer_id) != 0:
    #         offer_obj = VendorOfferData.objects.get(id=int(offer_id))
    #         offer_less = round((sub_total * float(offer_obj.percentage)) / 100, 2)
    #     custom_id = 'M4A00' + str(order_obj.id)
    #     settings_obj = ChargeSettingData.objects.first()
    #
    #     if not is_home_delivery:
    #         delivery_charge = 0
    #     else:
    #         delivery_charge = static_bill_function(miles=float(miles))
    #     # delivery_charge = static_bill_function(miles=float(miles))
    #
    #     if sub_total >= 40:
    #         delivery_charge = 0
    #     else:
    #         delivery_charge = static_bill_function(miles=float(miles))
    #
    #     if delivery_charge <= 3:
    #         driver_charge = settings_obj.driver_charge_upto_3_mile
    #     elif 3 < delivery_charge <= 5:
    #         driver_charge = settings_obj.driver_charge_from_3to5_mile
    #     elif 5 < delivery_charge <= 10:
    #         driver_charge = settings_obj.driver_charge_from_5to10_mile
    #     else:
    #         driver_charge = settings_obj.driver_charge_from_10to30_mile
    #
    #     packing_charge_per = settings_obj.packing_charge_per
    #
    #     if float(total_quatity) > 4:
    #         if float(total_quatity) % 4 == 0:
    #             sub_quatity_diffrence = float(total_quatity) // 4
    #         else:
    #             sub_quatity_diffrence = 1 + float(total_quatity) // 4
    #     else:
    #         sub_quatity_diffrence = 1
    #     print(sub_quatity_diffrence)
    #     print(packing_charge_per)
    #     packing_charge = sub_quatity_diffrence * float(packing_charge_per)
    #
    #     total_bill = (float(sub_total) + float(packing_charge) + float(delivery_charge) - float(offer_less))
    #     skype_amount = ((float(total_bill) * 1.40 / 100) + 0.20)
    #
    #     order_obj.skype_amount = round(skype_amount, 4)
    #     order_obj.total_bill = total_bill
    #     order_obj.supplier_fee = supplier_fee
    #     order_obj.discount = offer_less
    #     order_obj.total = sub_total
    #     order_obj.mile_charge = delivery_charge
    #     order_obj.driver_charge = driver_charge
    #     order_obj.packing_charge = packing_charge
    #     order_obj.custom_id = custom_id
    #     order_obj.offer_id = offer_id
    #     order_obj.latitude = order_obj.vendor.latitude
    #     order_obj.longitude = order_obj.vendor.longitude
    #     order_obj.save()
    #
    #     notification_data = {
    #         'order': order_obj.id,
    #         'type': 'created'
    #     }
    #
    #     try:
    #         # Notify driver that he received the request
    #         notify_text = "Dear " + str(vendor_obj.shop_name) + ", You have a New " \
    #                                                             "order with order ID : " + str(
    #             order_obj.custom_id) + ' .'
    #         notify_user(
    #             user=vendor_obj.user,
    #             message=notify_text,
    #             title='New Order Received',
    #             data=notification_data)
    #
    #     except Exception as e:
    #         print(str(e))
    #
    #     return JsonResponse({'success': True, 'cart_list': cart_list, 'total_bill': float(total_bill),
    #                          'total': sub_total,
    #                          'offer_id': offer_id,
    #                          'vendor_id': vendor_obj.id,
    #                          'shop_name': vendor_obj.shop_name,
    #                          'order_id': order_obj.id,
    #                          'supplier_fee': supplier_fee,
    #                          'offer_discount': offer_less,
    #                          'packing_charge': packing_charge,
    #                          'driver_charge': driver_charge})


# @csrf_exempt
# def place_order_new(request):


def is_valid_queryparam(param):
    return param != '' and param is not None


@login_required()
def search_List(request):
    base_image_url = request.scheme + '://' + request.get_host()
    user_id = request.GET.get('id')
    if user_id is None:
        order_list = OrderData.objects.all().order_by('-created')
    else:
        order_list = OrderData.objects.filter(user__id=int(user_id)).order_by('-created')
    order_id_contains_query = request.GET.get('order_id')
    vendor_contains_query = request.GET.get('vendor_contains')
    start_date = request.GET.get('date_min')
    end_date = request.GET.get('date_max')
    status_contains_query = request.GET.get('status_contains')

    if is_valid_queryparam(order_id_contains_query):
        order_list = order_list.filter(custom_id__icontains=order_id_contains_query)

    elif is_valid_queryparam(vendor_contains_query) \
            and is_valid_queryparam(start_date) and is_valid_queryparam(end_date) \
            and is_valid_queryparam(status_contains_query):
        order_list = order_list.filter(vendor__shop_name__icontains=vendor_contains_query)
        order_list = order_list.filter(order_date__range=[start_date, end_date])
        order_list = order_list.filter(status__icontains=status_contains_query)
    #
    # elif status_contains_query != '' and status_contains_query != '0' \
    #         and is_valid_queryparam(vendor_contains_query):
    #     order_list = order_list.filter(vendor__shop_name__icontains=vendor_contains_query)
    #     order_list = order_list.filter(status__icontains=status_contains_query)

    # elif is_valid_queryparam(status_contains_query) \
    #         and start_date != '' and start_date is not None:
    #     qs = qs.filter(status__icontains=status_contains_query)
    #     qs = qs.filter(order_date__range=[start_date, end_date])

    elif is_valid_queryparam(vendor_contains_query):
        order_list = order_list.filter(vendor__shop_name__icontains=vendor_contains_query)
    elif is_valid_queryparam(start_date) and is_valid_queryparam(end_date):
        order_list = order_list.filter(order_date__range=[start_date, end_date])
    elif is_valid_queryparam(status_contains_query):
        order_list = order_list.filter(status__icontains=status_contains_query)
    else:
        order_list = OrderData.objects.all().order_by('-created')
    order_list = OrderListSerializer(order_list, many=True)
    order_list_qs = order_list.data
    paginator = Paginator(order_list_qs, 1000)
    page = request.GET.get('page')
    try:
        order_list_qs = paginator.page(page)
    except PageNotAnInteger:
        order_list_qs = paginator.page(1)
    except EmptyPage:
        order_list_qs = paginator.page(paginator.num_pages)
    # order_list = OrderListSerializer(order_list, many=True)
    # json_data = JSONRenderer().render(order_list.data)
    # print(json_data)
    # print(order_list.data)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'order/search.html', {
        'order_list': order_list_qs,
        'tab': int(tab),
        # 'queryset': order_list,
        'base_image_url': base_image_url})


@login_required
def order_data_dashboard(request):
    base_image_url = request.scheme + '://' + request.get_host()
    user_id = request.GET.get('id')
    if user_id is None:
        order_list = OrderData.objects.all().order_by('-created')
    else:
        order_list = OrderData.objects.filter(user__id=int(user_id)).order_by('-created')

    vendors = VendorData.objects.all()
    order_list = OrderListSerializer(order_list, many=True)
    order_list_qs = order_list.data
    paginator = Paginator(order_list_qs, 20)
    page = request.GET.get('page')
    try:
        order_list_qs = paginator.page(page)
    except PageNotAnInteger:
        order_list_qs = paginator.page(1)
    except EmptyPage:
        order_list_qs = paginator.page(paginator.num_pages)
    # order_list = OrderListSerializer(order_list, many=True)
    # json_data = JSONRenderer().render(order_list.data)
    # print(json_data)
    # print(order_list.data)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'order/order_list.html', {
        'order_list': order_list_qs,
        'tab': int(tab),
        'vendors': vendors,
        # 'queryset': order_list,
        'base_image_url': base_image_url})



@login_required
def payout_data_dashboard(request):
    base_image_url = request.scheme + '://' + request.get_host()
    user_id = request.GET.get('id')
    if user_id is None:
        order_list = PaymentHistoryData.objects.all().order_by('-payout_date')
    else:
        order_list = PaymentHistoryData.objects.filter(contact_id=int(user_id)).order_by('-payout_date')

    vendors = VendorData.objects.all()
    order_list = order_list
    order_list_qs = order_list
    paginator = Paginator(order_list_qs, 20)
    page = request.GET.get('page')
    try:
        order_list_qs = paginator.page(page)
    except PageNotAnInteger:
        order_list_qs = paginator.page(1)
    except EmptyPage:
        order_list_qs = paginator.page(paginator.num_pages)
    # order_list = OrderListSerializer(order_list, many=True)
    # json_data = JSONRenderer().render(order_list.data)
    # print(json_data)
    # print(order_list.data)
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'payout/order_list.html', {
        'order_list': order_list_qs,
        'tab': int(tab),
        'vendors': vendors,
        # 'queryset': order_list,
        'base_image_url': base_image_url})


# @login_required
# def order_data_dashboard(request):
#     base_image_url = request.scheme + '://' + request.get_host()
#     user_id = request.GET.get('id')
#     if user_id is None:
#         order_list = OrderData.objects.all().order_by('-created')
#     else:
#         order_list = OrderData.objects.filter(user__id=int(user_id)).order_by('-created')
#
#     paginator = Paginator(order_list, 20)
#     page = request.GET.get('page')
#
#     try:
#         order_list = paginator.page(page)
#     except PageNotAnInteger:
#         order_list = paginator.page(1)
#     except EmptyPage:
#         order_list = paginator.page(paginator.num_pages)
#     order_list = OrderListSerializer(order_list, many=True)
#     tab = request.GET.get('tab')
#     if tab == '' or tab is None:
#         tab = 1
#     return render(request, 'order/order_list.html', {'message': 'order_list',
#                                                      'tab': int(tab),
#                                                      'order_list': order_list.data, 'base_image_url': base_image_url})


def order_details_dashboard(request, order_id):
    base_image_url = request.scheme + '://' + request.get_host()
    order_obj = OrderData.objects.get(id=int(order_id))
    tab = request.GET.get('tab')
    if tab == '' or tab is None:
        tab = 1
    return render(request, 'order/order_details.html', {'message': 'order_list',
                                                        'tab': int(tab),
                                                        'order_obj': order_obj, 'base_image_url': base_image_url})


@csrf_exempt
def order_accept(request, order_id):
    if request.method == 'POST':
        order_obj = OrderData.objects.get(id=int(order_id))
        # order_queryset = OrderServices.objects.filter(order=order_obj).values_list('service__id')

        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        sub_total = 0
        total_quatity = 0
        offer_less = 0
        supplier_fee = 0
        order_sub_cat_id_list = data['order_sub_cat_id_list']
        offer_id = order_obj.offer_id
        for obj in order_sub_cat_id_list:
            print('ooooooooooooooooooooo', obj['is_available'])
            order_service_obj = OrderServices.objects.get(id=int(obj['id']))

            if not obj['is_available']:
                order_service_obj.is_removed = True
                order_service_obj.save()
            else:
                total_quatity += float(obj['quantity'])
                sub_cat_obj = VendorSubCategoryData.objects.get(id=order_service_obj.service.id)
                print(sub_cat_obj.offer_price, 'aaaaaaaaaaaaaaaaaaaaaaaaaa')

                if float(sub_cat_obj.offer_price) != 0.0:
                    item_price = float(order_service_obj.quantity) * float(sub_cat_obj.offer_price)
                else:
                    item_price = float(order_service_obj.quantity) * float(sub_cat_obj.price)

                sub_total += float(item_price)

        if int(offer_id) != 0:
            offer_obj = VendorOfferData.objects.get(id=int(offer_id))
            offer_less = round((sub_total * float(offer_obj.percentage)) / 100, 2)

        settings_obj = ChargeSettingData.objects.first()

        delivery_charge = order_obj.mile_charge

        packing_charge_per = settings_obj.packing_charge_per
        packing_charge = packing_charge_per

        # if float(total_quatity) > 4:
        #     if float(total_quatity) % 4 == 0:
        #         sub_quatity_diffrence = float(total_quatity) // 4
        #     else:
        #         sub_quatity_diffrence = 1 + float(total_quatity) // 4
        # else:
        #     sub_quatity_diffrence = 1
        # print(sub_quatity_diffrence)
        # print(packing_charge_per)
        # packing_charge = sub_quatity_diffrence * float(packing_charge_per)

        total_bill = (float(sub_total) + float(packing_charge) + float(delivery_charge) - float(offer_less))

        order_obj.total_bill = total_bill
        order_obj.supplier_fee = supplier_fee
        order_obj.discount = offer_less
        order_obj.total = sub_total
        order_obj.packing_charge = packing_charge
        # order_obj.mile_charge = miles_charge
        order_obj.status = 2
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'accept'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name) \
                          + ", \nYour Order ID: " + str(order_obj.custom_id) \
                          + ' has been accepted by "' + str(order_obj.vendor.shop_name) \
                          + '". Please make the payment in 15 mins to process your order.\n- Meats4all Team'
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Order Accepted',
                data=notification_data)

        except Exception as e:
            print(str(e))

        message = "Dear " + str(order_obj.user.first_name) \
                  + ", \nYour Order ID: " + str(order_obj.custom_id) \
                  + ' has been accepted by "' + str(order_obj.vendor.shop_name) \
                  + '". Please make the payment in 15 mins to process your order.\n- Meats4all Team'
        try:
            send_sms(order_obj.user.username, message)
        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Accepted',
        })


@csrf_exempt
def order_reject(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            reason = data['reason']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.status = 3
        order_obj.cancel_reason = reason
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'reject'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name) + ", Your" \
                                                                     "Order with order ID: " + str(
                order_obj.custom_id) + \
                          '  has been rejected due to temporarily closed.'
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Order Rejected',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Rejected',
        })


@csrf_exempt
def payment_initiate(request, order_id):
    order_obj = OrderData.objects.get(id=int(order_id))
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        payment_status = data['payment_status']
        stripe_payment_id = data['transaction_id']
        if payment_status == 3:
            order_obj.payment_status = payment_status
            order_obj.save()
            return JsonResponse({'success': True, 'payment_status': payment_status})

        order_obj.status = 4
        order_obj.payment_status = payment_status
        order_obj.stripe_payment_id = stripe_payment_id
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'ready_to_dispatch'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name) \
                          + ", \nYour Order with ID: " + str(order_obj.custom_id) \
                          + " payment has been completed. Order is in progress.\n- Meats4all Team"
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Payment Complete',
                data=notification_data)

        except Exception as e:
            print(str(e))

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.vendor.shop_name) \
                          + ",\nYour Order with order ID: " + str(order_obj.custom_id) \
                          + " has been paid. Please keep the items ready for dispatch.\n- Meats4all Team"
            notify_user(
                user=order_obj.vendor.user,
                message=notify_text,
                title='Payment Complete',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            'payment_status': payment_status,
            "message": 'Order InProgress',
        })


@csrf_exempt
def order_pickup(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.status = 5
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'pickup'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name)\
                          + ", \nYour Order with order ID: " \
                          + str(order_obj.custom_id) \
                          + " has been picked up. Our delivery partner " \
                          "on his way to deliver your order."
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Order Picked Up',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Picked Up',
        })


@csrf_exempt
def order_delivered(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.status = 6
        order_obj.save()

        response = static_amount_breakup(order_id)

        notification_data = {
            'order': order_obj.id,
            'type': 'delivered'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name) \
                          + ", \nYour Order with order ID: " + str(order_obj.custom_id) \
                          + " has been delivered successfully. Please give a star to our delivery partner " \
                            "and meat provider.\n- Meats4all Team"
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Order Delivered',
                data=notification_data)

        except Exception as e:
            print(str(e))

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.vendor.user.first_name) \
                          + ", \nOrder with order ID: " + str(order_obj.custom_id) \
                          + " has been delivered successfully.\n- Meats4all Team"
            notify_user(
                user=order_obj.vendor.user,
                message=notify_text,
                title='Order Delivered',
                data=notification_data)

        except Exception as e:
            print(str(e))

        # str_list = '\n'
        order_services = OrderServices.objects.filter(order=order_obj, is_removed=False)
        # if order_obj.is_home_delivery:
        sms_text = 'Dear ' + str(order_obj.user.first_name) + ',' \
                                                              '\nYour Order ID: ' + str(order_obj.custom_id) + \
                   ' has been successfully delivered. ' \
                   '\nPlease check your email for the detail receipt. ' \
                   '\nWe look forward to your next order soon and ' \
                   'get further discounts.\n- Meats4all Team'
        #     try:
        #         send_sms(order_obj.user.username, sms_text)
        #     except Exception as e:
        #         print(str(e))
        # else:
        #     sms_text = 'Dear ' + str(order_obj.user.first_name) + ',' \
        #                                                           '\nYour Order ID: ' + str(order_obj.custom_id) + \
        #                ' has been successfully collected. ' \
        #                '\nPlease check your email for the detail receipt. ' \
        #                '\nWe look forward to your next order soon and ' \
        #                'avail further discounts.\n- Meats4all Team'
        #     try:
        #         send_sms(order_obj.user.username, sms_text)
        #     except Exception as e:
        #         print(str(e))

        customer_message_for_mail = 'Dear Valued Customer, \nThank you for placing your order with Meats4all. ' \
                                    '\nWe hope all your ordered items have been delivered as per your required ' \
                                    'specifications. \nIf there are any issues with the order or missing items, ' \
                                    'please contact the Vendor directly on their respective contact number.\n We ' \
                                    'look forward to your next order soon and redeem further discounts.'

        vendor_message_for_mail = 'Dear ' + str(order_obj.vendor.shop_name) + ',\n Thank you for ' \
                                                                              'successfully delivering order no ' + str(
            order_obj.custom_id) + '. Please find the ' \
                                   'order details with itemized bill as below Bill Details: '

        customer_signature = 'Thank You for shopping with us. \nTeam Meats4all'
        vendor_signature = 'Thank you for your continued support and business.\nTeam Meats4all'
        customer_bottom_text = 'If there are any issues with the order or missing items, ' \
                               'the customer will contact you directly on your registered contact number.'

        vendor_bottom_text = 'If there are any issues with the order or missing items, ' \
                             'the customer will contact you directly on your registered contact number. ' \

        customer_html_content = render_to_string('email_template.html',
                                                 {'text': customer_message_for_mail,
                                                  'order_services_list': order_services,
                                                  'order_obj': order_obj,
                                                  'customer_bottom_text': customer_bottom_text,
                                                  'signature': customer_signature
                                                  })

        vendor_html_content = render_to_string('email_template.html',
                                               {'text': vendor_message_for_mail,
                                                'order_services_list': order_services,
                                                'order_obj': order_obj,
                                                'bottom_text': vendor_bottom_text,
                                                'signature': vendor_signature
                                                })

        try:

            send_email(to=order_obj.user.email, subject="Order Delivered", body=customer_html_content)
        except Exception as e:
            print(str(e))

        try:
            send_email(to=order_obj.vendor.email, subject="Order Delivered", body=vendor_html_content)
        except Exception as e:
            print(str(e))

        try:
            send_sms(order_obj.user.username, sms_text)
        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Delivered',
        })


@csrf_exempt
def order_assigned(request, order_id):
    if request.method == 'POST':
        proximity = SettingData.objects.get(key='EXECUTIVE_PROXIMITY')
        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.status = 8
        order_obj.save()

        if order_obj.is_home_delivery:
            nearby_executives = ExecutiveData.objects.nearby(
                latitude=order_obj.vendor.latitude,
                longitude=order_obj.vendor.longitude,
                proximity=proximity.value
            ).filter(
                is_verified=True,
                is_available=True
            )

            if nearby_executives.count() == 0:
                return JsonResponse({'success': True,
                                     'executives': [],
                                     # 'offers': []
                                     })

            notification_data = {
                'order': order_obj.id,
                'type': 'received'
            }

            for obj in nearby_executives:
                try:
                    # Notify driver that he received the request
                    notify_text = "Dear " + str(
                        obj.user.first_name) + ", You have received a new order request with order ID : " + str(
                        order_obj.custom_id) + '.'
                    notify_user(
                        user=obj.user,
                        message=notify_text,
                        title='New Order Received',
                        data=notification_data)

                except Exception as e:
                    print(str(e))
                #
                # try:
                #     # Notify driver that he received the request
                #     notify_text = "Dear " + str(order_obj.vendor.user.first_name) + ", " \
                #                                                                     "Order with order " \
                #                                                                     "ID: " + str(order_obj.custom_id) + \
                #                   " has been accepted successfully .\n- Meats4all Team"
                #     notify_user(
                #         user=order_obj.vendor.user,
                #         message=notify_text,
                #         title='Order Accepted',
                #         data=notification_data)
                #
                # except Exception as e:
                #     print(str(e))
            serializer = ExecutiveListSerializer(nearby_executives, many=True)

            notification_data = {
                'order': order_obj.id,
                'type': 'ready_to_dispatch'
            }

            try:
                if order_obj.is_home_delivery:
                    # Notify driver that he received the request
                    notify_text = "Dear " + str(order_obj.user.first_name) + ", \nYour Order with Order ID: " \
                                  + str(order_obj.custom_id) + 'has been dispatched. Our delivery partner on' \
                                                               ' his way to receive your order.\n- Meats4all Team'
                else:
                    # Notify driver that he received the request
                    notify_text = "Dear " + str(order_obj.user.first_name) + ", \nYour Order with Order ID: " \
                                  + str(order_obj.custom_id) + ' is ready to collect at' \
                                  + str(order_obj.vendor.shop_name) + '.\n- Meats4all Team'
                notify_user(
                    user=order_obj.user,
                    message=notify_text,
                    title='Order Dispatched',
                    data=notification_data)

            except Exception as e:
                print(str(e))

            try:
                if order_obj.is_home_delivery:
                    sms_text = "Dear " + str(order_obj.user.first_name) + ", \nYour Order with Order ID: " \
                                      + str(order_obj.custom_id) + 'has been dispatched. Our delivery partner on' \
                                                                   ' his way to receive your order.\n- Meats4all Team'
                else:
                    sms_text = "Dear " + str(order_obj.user.first_name) + ", \nYour Order with Order ID: " \
                                  + str(order_obj.custom_id) + ' is ready to collect at' \
                                  + str(order_obj.vendor.shop_name) + '.\n- Meats4all Team'
                send_sms(order_obj.user.username, sms_text)
            except Exception as e:
                print(str(e))

            return JsonResponse({
                "error": False,
                "executives": serializer.data,
                "message": 'Order Executive List',
            })

        else:
            return JsonResponse({
                "error": False,
                "message": 'Order InProgress',
            })


@csrf_exempt
def accept_executive(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        try:
            token = data['token']
            decoded_token = jwt_decode(token)
        except Exception as e:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        executive_id = data['executive_id']
        executive_obj = ExecutiveData.objects.get(id=int(executive_id))
        order_obj = OrderData.objects.get(id=int(order_id))
        # print(executive_obj)
        # print(order_obj.user.first_name)
        # print(order_obj.delivery_boy.first_name)
        if order_obj.delivery_boy is not None:
            return JsonResponse({'success': False, 'message': 'This Order has been accepted by another executive'})
        order_obj.delivery_boy = executive_obj
        order_obj.assigned_executive = True
        order_obj.driver_amount_per = executive_obj.de_charge_per
        order_obj.save()

        notification_data = {
            'order': order_obj.id,
            'type': 'accept_by_executive'
        }

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.user.first_name) + ", \nYour Order with order ID: " \
                          + str(order_obj.custom_id) + ' has been assigned to ' \
                          + str(order_obj.delivery_boy.user.first_name) + '.\n- Meats4all Team'
            notify_user(
                user=order_obj.user,
                message=notify_text,
                title='Order Assigned',
                data=notification_data)

        except Exception as e:
            print(str(e))

        try:
            # Notify driver that he received the request
            notify_text = "Dear " + str(order_obj.vendor.shop_name) + "Order with order ID: " + \
                          str(order_obj.custom_id) + ' has been accepted by our trusted delivery partner ' + \
                          str(order_obj.delivery_boy.user.first_name) + ' with his/her vehicle no: ' + \
                          str(order_obj.delivery_boy.v_name) + '.'
            notify_user(
                user=order_obj.vendor.user,
                message=notify_text,
                title='Order Accepted',
                data=notification_data)

        except Exception as e:
            print(str(e))

        return JsonResponse({
            "error": False,
            "order_id": order_obj.id,
            "message": 'Order Accepted by Executive',
        })


@csrf_exempt
def delivery_image(request, order_id):
    if request.method == 'POST':
        image = request.FILES.get('image')
        token = request.POST.get('token')
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})
        order_obj = OrderData.objects.get(id=int(order_id))
        order_obj.delivery_image = image
        order_obj.save()
        base_image_url = request.scheme + '://' + request.get_host()
        image = base_image_url + '/media/' + str(image)
        return JsonResponse({'success': True, 'message': 'Image Updated successfully', 'image': str(image)})


@csrf_exempt
def rating(request, order_id):
    if request.method == 'POST':
        data = json.loads(request.body)
        token = data['token']
        executive_rating = data['executive_rating']
        executive_text = data['executive_text']
        vendor_rating = data['vendor_rating']
        vendor_text = data['vendor_text']
        decoded_token = jwt_decode(token)
        mobile = decoded_token['mobile']
        user = User.objects.filter(username=mobile).first()
        if not user:
            return JsonResponse({'success': False, 'message': 'Invalid User'})

        obj = OrderData.objects.get(id=int(order_id))
        obj.executive_feedback = executive_text
        obj.executive_rating = executive_rating
        obj.vendor_feedback = vendor_text
        obj.vendor_rating = vendor_rating
        obj.save()
    return JsonResponse({'success': True, 'rating': 'You have rated successfully'})


def static_amount_breakup(order_id):
    order_obj = OrderData.objects.get(id=int(order_id))

    vendor_per = order_obj.vendor.meat4all_per
    if order_obj.is_home_delivery:
        executive_per = order_obj.delivery_boy.de_charge_per
    else:
        executive_per = 0
    # executive_per = order_obj.delivery_boy.de_charge_per
    order_obj.vendor_amount_per = vendor_per
    order_obj.driver_amount_per = executive_per
    order_obj.save()

    try:
        meat4all_amount = (float(order_obj.total) * float(vendor_per)) / 100
    except Exception as e:
        meat4all_amount = 0

    try:
        meat4all_driver_amount = (float(order_obj.mile_charge) * float(executive_per)) / 100
    except Exception as e:
        meat4all_driver_amount = 0

    vendor_amount = float(order_obj.total) - float(meat4all_amount)
    driver_amount = float(order_obj.mile_charge) - float(meat4all_driver_amount)
    order_obj.meat4all_amount = round(meat4all_amount, 4)
    order_obj.vendor_amount = vendor_amount
    order_obj.driver_amount = driver_amount
    order_obj.meat4all_driver_amount = meat4all_driver_amount
    order_obj.save()
    return True

# @login_required
# def executive_report(request, order_id):
#     base_image_url = request.scheme + '://' + request.get_host()
#     executive_report = OrderData.objects.get(id=int(order_id))
#     tab = request.GET.get('tab')
#     if tab == '' or tab is None:
#         tab = 1
#     return render(request, 'executive/executive_report.html', {'message': 'executive_report',
#                                                              'tab': int(tab),
#                                                              'executive_report': executive_report,'base_image_url': base_image_url})
