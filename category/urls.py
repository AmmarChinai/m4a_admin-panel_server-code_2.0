
from django.urls import path

from category.views import *

urlpatterns = [

    #dashboad
    path('category_list/', category_data_dashboard),
    path('sub_category_list/', sub_category_data_dashboard),

    path('order_list/', order_data_dashboard),
    path('payout_list/', payout_data_dashboard),
    path('search/', search_List),
    path('view_order/<int:order_id>/', order_details_dashboard),

    #app
    path('sub_category_data/', sub_category_data),
    path('<int:category_id>/sub_category/<int:vendor_id>/', category_sub_category_data),
    path('category_data/', category_data),

    path('add_cart/', add_cart),
    path('place_order/', place_order),
    # path('place_order_new/', place_order_new),

    path('order/<int:order_id>/rating/', rating),
    path('order/<int:order_id>/payment/', payment_initiate),
    path('order/<int:order_id>/accept/', order_accept),
    path('order/<int:order_id>/reject/', order_reject),
    path('order/<int:order_id>/assigned/', order_assigned),

    path('order/<int:order_id>/pickup/', order_pickup),
    path('order/<int:order_id>/delivered/', order_delivered),
    path('order/<int:order_id>/delivery_image/', delivery_image),

    path('order/<int:order_id>/accept_executive/', accept_executive),

]
