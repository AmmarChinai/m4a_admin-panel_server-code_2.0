from django import forms

from category.models import SubCategoryData


class SubCategoryForm(forms.ModelForm):
	class Meta:
		model = SubCategoryData
		fields = ('name', 'category')


class UpdateSubCategoryForm(forms.ModelForm):
	class Meta:
		model = SubCategoryData
		fields = ('name', 'category')


