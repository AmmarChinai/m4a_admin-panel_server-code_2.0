# Generated by Django 2.0 on 2020-10-15 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0020_auto_20200813_1206'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdata',
            name='booking_slot',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AddField(
            model_name='orderservices',
            name='note',
            field=models.TextField(blank=True, default=''),
        ),
    ]
