# Generated by Django 2.0 on 2020-07-15 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0012_auto_20200704_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdata',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'Created'), (2, 'Accepted'), (3, 'Rejected'), (4, 'In-Progress'), (5, 'Picked Up'), (6, 'Delivered'), (7, 'Cancelled'), (8, 'Ready To Dispatch')], default=1),
        ),
    ]
