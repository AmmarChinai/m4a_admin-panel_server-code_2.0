import datetime

from rest_framework import serializers

from category.models import VendorSubCategoryData, VendorOfferData, CategoryData, SubCategoryData, OrderData, \
    OrderServices
from user.models import User, VendorData, ExecutiveData, AddressData


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name',
                  'email', 'gender', 'avatar', 'source', 'latitude', 'longitude')


class VendorSubDetailSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    # sub_category = VendorSubCategorySerializer(source='vendorsubcategorydata_set', many=True)

    class Meta:
        model = VendorData
        fields = ('id', 'user', 'latitude', 'shop_name', 'longitude', 'logo', 'address', 'city',
                  'rating', 'end_time', 'mobile2', 'start_time', 'email')


class ExecutiveListSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = ExecutiveData
        fields = '__all__'


class OfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = VendorOfferData
        fields = ('id', 'code', 'name', 'description', 'percentage')


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryData
        fields = ('id', 'name', 'image')


class SubCategorySerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)

    class Meta:
        model = SubCategoryData
        fields = ('id', 'name', 'image', 'category')


class VendorSubCategoryDataSerializer(serializers.ModelSerializer):
    sub_category = SubCategorySerializer(read_only=True)

    class Meta:
        model = VendorSubCategoryData
        fields = ('id', 'sub_category', 'price', 'offer_price', 'quantity', 'unit', 'is_available')


class VendorSubCategorySerializer(serializers.ModelSerializer):
    vendor = VendorSubCategoryDataSerializer(read_only=True)

    class Meta:
        model = VendorSubCategoryData
        fields = ('id', 'vendor', 'price', 'offer_price', 'quantity', 'unit', 'is_available')


class VendorCategorySerializer(serializers.ModelSerializer):
    vendor = VendorSubDetailSerializer(read_only=True)
    sub_category = CategorySerializer(read_only=True)

    class Meta:
        model = VendorSubCategoryData
        fields = ('id', 'vendor', 'sub_category')


class VendorOfferSerializer(serializers.ModelSerializer):
    end = serializers.SerializerMethodField()

    class Meta:
        model = VendorOfferData
        fields = ('id', 'code', 'name', 'percentage', 'start', 'end', 'description')

    def get_end(self, obj):
        data = {}
        if obj.end >= datetime.datetime.today().date():
            data['is_available'] = True
            data['end'] = obj.end
        else:
            data['is_available'] = False
            data['end'] = obj.end
        return data


class VendorDetailSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    sub_category = VendorSubCategoryDataSerializer(source='vendorsubcategorydata_set', many=True)
    offer = VendorOfferSerializer(source='vendorofferdata_set', many=True)

    class Meta:
        model = VendorData
        fields = ('id', 'user', 'latitude', 'shop_name', 'longitude', 'sub_category', 'logo', 'address', 'city',
                  'rating', 'end_time', 'mobile2', 'start_time', 'email', 'offer')


class ExecutiveDetailSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = ExecutiveData
        fields = ('id', 'user', 'latitude', 'longitude', 'address', 'city', 'v_name', 'dl_no', 'dl_date', 'mobile2',
                  'rating', 'rc', 'email')


class VendorListSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    offer = VendorOfferSerializer(source='vendorofferdata_set', many=True)

    class Meta:
        model = VendorData
        fields = ('id', 'user', 'latitude', 'longitude', 'logo', 'address', 'city', 'shop_name',
                  'rating', 'offer')


class OrderServicesSerializer(serializers.ModelSerializer):
    # user = UserSerializer(read_only=True)
    sub_category = serializers.SerializerMethodField()
    # serializers.ReadOnlyField(source='service.sub_category.name')
    # status = serializers.SerializerMethodField()

    class Meta:
        model = OrderServices
        fields = ('id', 'sub_category', 'quantity', 'unit', 'price', 'is_removed', 'note')

    def get_sub_category(self, obj):
        data = {}
        data['sub_category_name'] = obj.service.sub_category.name
        data['sub_category_id'] =  obj.service.sub_category.id
        data['vendor_sub_category_id'] =  obj.service.id
        data['category_id'] =  obj.service.sub_category.category.id
        return data


class VendorDetailOrderListSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    # sub_category = VendorSubCategoryDataSerializer(source='vendorsubcategorydata_set', many=True)
    # offer = VendorOfferSerializer(source='vendorofferdata_set', many=True)

    class Meta:
        model = VendorData
        fields = ('id', 'user', 'latitude', 'shop_name', 'longitude', 'logo', 'address', 'city',
                  'rating', 'end_time', 'mobile2', 'start_time', 'email', 'meat4all_per')


class OrderListSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    vendor = VendorDetailOrderListSerializer(read_only=True)
    status = serializers.SerializerMethodField()
    payout_status = serializers.SerializerMethodField()
    driver_payout_status = serializers.SerializerMethodField()
    order = OrderServicesSerializer(source='orderservices_set', many=True)
    created = serializers.DateTimeField(format="%b %d, %Y - %I:%M %p")
    modified = serializers.DateTimeField(format="%b %d, %Y - %I:%M %p")
    order_date = serializers.DateField(format="%b %d, %Y")

    class Meta:
        model = OrderData
        fields = '__all__'

    def get_status(self, obj):
        data = {}
        data['status'] = obj.status
        data['status_text'] = obj.get_status_display()
        # data['meat4all_amount'] = '£ ' + str(float(obj.total_bill) - float(obj.supplier_fee))
        # data['vendor_amount'] = '£ ' + str(float(obj.supplier_fee) + float(obj.packing_charge))
        return data
    def get_payout_status(self, obj):
        data = {}
        data['status'] = obj.payout_status
        data['status_text'] = obj.get_payout_status_display()
        return data

    def get_driver_payout_status(self, obj):
        data = {}
        data['status'] = obj.driver_payout_status
        data['status_text'] = obj.get_driver_payout_status_display()
        return data





class OrderDetailsSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    vendor = VendorDetailOrderListSerializer(read_only=True)
    delivery_boy = ExecutiveDetailSerializer(read_only=True)
    status = serializers.SerializerMethodField()
    payout_status = serializers.SerializerMethodField()
    payment_status = serializers.SerializerMethodField()
    order = OrderServicesSerializer(source='orderservices_set', many=True)
    created = serializers.DateTimeField(format="%b %d, %Y - %I:%M %p")
    modified = serializers.DateTimeField(format="%b %d, %Y - %I:%M %p")
    order_date = serializers.DateField(format="%b %d, %Y")


    class Meta:
        model = OrderData
        fields = '__all__'


    def get_status(self, obj):
        data = {}
        data['status'] = obj.status
        data['status_text'] = obj.get_status_display()
        return data

    def get_payout_status(self, obj):
        data = {}
        data['status'] = obj.payout_status
        data['status_text'] = obj.get_payout_status_display()
        return data

    def get_payment_status(self, obj):
        data = {}
        data['status'] = obj.payment_status
        data['status_text'] = obj.get_payment_status_display()
        return data


class AddressSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = AddressData
        fields = '__all__'


