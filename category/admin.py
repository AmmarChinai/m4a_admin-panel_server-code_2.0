from django.contrib import admin

# Register your models here.

from category.models import *


class CategoryDataAdmin(admin.ModelAdmin):
	list_display = ['name', 'is_active', 'image']

admin.site.register(CategoryData,CategoryDataAdmin)


class SubCategoryDataAdmin(admin.ModelAdmin):
	list_display = ['name', 'category', 'is_active', 'image']

admin.site.register(SubCategoryData, SubCategoryDataAdmin)



class VendorSubCategoryDataAdmin(admin.ModelAdmin):
	list_display = ['sub_category', 'unit', 'price', 'is_available', 'quantity', 'vendor']

admin.site.register(VendorSubCategoryData, VendorSubCategoryDataAdmin)


class VendorOfferDataAdmin(admin.ModelAdmin):
	list_display = ['vendor', 'code', 'name', 'start', 'end', 'percentage']

admin.site.register(VendorOfferData, VendorOfferDataAdmin)


class SettingDataAdmin(admin.ModelAdmin):
	list_display = ['key', 'value']

admin.site.register(SettingData, SettingDataAdmin)


class OrderDataAdmin(admin.ModelAdmin):
	list_display = ['id', 'vendor', 'user']

admin.site.register(OrderData, OrderDataAdmin)


class OrderServicesAdmin(admin.ModelAdmin):
	list_display = ['id', 'service', 'order', 'is_removed']

admin.site.register(OrderServices, OrderServicesAdmin)