from django.db import models

# Create your models here.
from django.db import models


class Invoice(models.Model):
    booking_id = models.PositiveIntegerField(null=True)
    case_id = models.CharField(max_length=30, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    mobile = models.CharField(max_length=20, null=True, blank=True)

    # Sum of all charges stored in charges table
    charges = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    # Sum of all taxes stored in invoice_taxes table
    tax = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)

    amount = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
    round_off = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    payable = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return "%s %s" % (self.id, self.name)



