from django.urls import path

from payment.views import create_payment_customer, payment_process, payment_web, create_connected_account_vendor

urlpatterns = [
    path('<int:order_id>/create_payment_customer/', create_payment_customer),
    path('create_connected_account_vendor/', create_connected_account_vendor),
    path('<int:order_id>/payment_process/', payment_process),
    path('payment_web/', payment_web),
]

