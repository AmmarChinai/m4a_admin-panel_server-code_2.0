# Generated by Django 2.0 on 2020-05-19 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('booking_id', models.PositiveIntegerField(null=True)),
                ('case_id', models.CharField(blank=True, max_length=30, null=True)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('mobile', models.CharField(blank=True, max_length=20, null=True)),
                ('charges', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('tax', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('amount', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('round_off', models.DecimalField(blank=True, decimal_places=2, max_digits=4, null=True)),
                ('payable', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
            ],
        ),
    ]
