import paypalrestsdk
import stripe
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from flask import json
from flask import jsonify

from category.models import OrderData
from delivey_app import settings


def create_payment_customer(request, order_id):
    order_obj = OrderData.objects.get(id=int(order_id))
    stripe.api_key = settings.SECRET_KEY_STRIPE
    if order_obj.user.stripe_customer_id is None or order_obj.user.stripe_customer_id == '':
        response = stripe.Customer.create(
            description="Test Customer",
        )
        order_obj.user.stripe_customer_id = response['id']
        order_obj.user.save()
    else:
        pass
    customer_id = order_obj.user.stripe_customer_id
    key = stripe.EphemeralKey.create(customer=customer_id, stripe_version='2020-08-27')
    print(key)

    intent = stripe.PaymentIntent.create(
        amount=123,
        currency='gbp',
    )
    print(intent)
    print('ppppppppppppppppppppppppp')
    client_secret = intent.client_secret
    print(client_secret)

    return JsonResponse({'success': True, 'empheral_key': key})


def payment_process(request, order_id):
    stripe.api_key = settings.SECRET_KEY_STRIPE
    order_obj = OrderData.objects.get(id=int(order_id))
    # print(order_obj)
    # print(order_obj.vendor.id)
    # print(order_obj.custom_id)
    # print(order_obj.vendor.shop_name)
    # print(order_obj.user.first_name)
    # print(order_obj.user.last_name)
    # print(order_obj.drop_address)

    c_f_name = order_obj.user.first_name
    c_l_name = order_obj.user.last_name
    o_id = order_obj
    c_id = order_obj.custom_id
    v_name = order_obj.vendor.shop_name
    v_id = order_obj.vendor.id
    c_address = order_obj.drop_address

    # stripe.Customer.create(
    #     description="My First Test Customer (created for API docs)",
    # )
    # print(round(order_obj.total_bill), 'sssssssssssssss')
    payment_bill = float(order_obj.total_bill) * 100

    intent = stripe.PaymentIntent.create(
        amount=round(payment_bill),
        currency='gbp',
        # metadata={'Customer Name': 'Ammar', 'Customer ID': order_id, 'Order_ID': order_obj.custom_id},
        metadata={'Customer Name': c_f_name + ' ' + c_l_name,
                  'Customer ID': o_id,
                  'Order ID': c_id,
                  'Vendor name': v_name,
                  'Vendor ID': v_id,
                  'Customer address': c_address,
                  },
    )
    client_secret = intent.client_secret

    return JsonResponse({'success': True, 'client_secret': str(client_secret)})


#
# def create_connected_account_vendor(request, order_id):
#     # order_obj = OrderData.objects.get(id=int(order_id))
#     stripe.api_key = settings.SECRET_KEY_STRIPE
#     # if order_obj.user.stripe_vendor_id is None or order_obj.user.stripe_vendor_id == '':
#     intent = stripe.Account.create(
#           type="custom",
#           country="UK",
#           email="jenny.rosen@example.com",
#           capabilities={
#             "card_payments": {"requested": True},
#             "transfers": {"requested": True},
#           },
#     )
#     client_secret = intent.client_secret
#
#     return JsonResponse({'success': True, 'client_secret': str(client_secret)})


@csrf_exempt
def payment_web(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        order_id = data['order_id']
        number = data['number']
        exp_month = data['exp_month']
        exp_year = data['exp_year']
        cvc = data['cvc']

        stripe.api_key = settings.SECRET_KEY_STRIPE
        order_obj = OrderData.objects.get(id=int(order_id))
        payment_bill = float(order_obj.total_bill) * 100

        intent = stripe.PaymentIntent.create(
            amount=round(payment_bill),
            currency='gbp',
        )
        client_secret = intent.client_secret

        payment_card = stripe.PaymentMethod.create(
            type="card",
            card={
                "number": number,
                "exp_month": exp_month,
                "exp_year": exp_year,
                "cvc": cvc,
            },
        )
        return JsonResponse({'success': True, 'client_secret': str(client_secret), 'payment_card': payment_card})


@csrf_exempt
def create_connected_account_vendor(request):
    if request.method == 'POST':
        stripe.api_key = settings.SECRET_KEY_STRIPE
        # order_obj = OrderData.objects.get(id=int(order_id))
        intent = stripe.Customer.delete_source(
          "cus_KbYicvec6ge5UT",
          "ba_1JwLb5BNAGTtTpMZAvz9o76x",
        )
        client_secret = intent.client_secret
        print(client_secret)
        return JsonResponse({'success': True, 'client_secret': str(client_secret)})
    # order_obj = OrderData.objects.get(id=int(order_id))
    # stripe.api_key = settings.SECRET_KEY_STRIPE
    # # if order_obj.user.stripe_vendor_id is None or order_obj.user.stripe_vendor_id == '':

    # # Create a PaymentIntent:
    # payment_intent = stripe.PaymentIntent.create(
    #     amount=10000,
    #     currency='gbp',
    #     payment_method_types=['card'],
    #     transfer_group='{ORDER10}',
    # )
    #
    # # Create a Transfer to a connected account (later):
    # transfer = stripe.Transfer.create(
    #     amount=7000,
    #     currency='gbp',
    #     destination='acct_1Jpt3TPfkMdnQ7VR',
    #     transfer_group='{ORDER10}',
    # )
    #
    # # Create a second Transfer to another connected account (later):
    # transfer = stripe.Transfer.create(
    #     amount=2000,
    #     currency='gbp',
    #     destination='{{OTHER_CONNECTED_STRIPE_ACCOUNT_ID}}',
    #     transfer_group='{ORDER10}',
    # )
    #
    # client_secret = intent.client_secret
    #
    # return JsonResponse({'success': True, 'client_secret': str(client_secret)})
